# main usage of this branch:


# trigMETAthenaAnalysisBasic 
This package is to perform a very basic MET trigger checks.  
This is an Athena based software framework.

## Main Functionalities
1. Create basic histograms of trigger and offline MET.
2. Print out trigger counts.
3. Produce a tree.
4. Produce a xAOD.
5. Produce plots from histograms.
6. Produce histograms from a tree.


# Getting Started
## Setup
```
mkdir trigMETAthenaAnalysis
cd trigMETAthenaAnalysis
mkdir source build run
cd source/
git clone ssh://git@gitlab.cern.ch:7999/zhelun/PUfit_studies.git
asetup AthAnalysis,21.2.18,here
cd ../
```
'kinit' may not be necessary when you are working on lxplus.  
The 'asetup' create a CMakeLists.txt under the source directory. For this, the asetup need to be done in the source directory with ",here".


## Build
```
cd build/
cmake ../source
source ${AthAnalysis_PLATFORM}/setup.sh
(source x86_64-slc6-gcc62-opt/setup.sh)
make
../
```

### Setup and Build with acm
```
mkdir build source
cd build
acmSetup AthAnalysis,21.2,latest
acm clone_project trigAthAnalysis atlas-inst-uvic/Trigger/trigMETAthenaAnalysisBasic
acm find_packages
acm compile

```


## Run
```
cd run/
mkdir testRun01
cd testRun01/
athena --filesInput="/hep300/data/khamano/data17/data17_13TeV.00338349.physics_Main.merge.AOD.f877_m1885/data17_13TeV.00338349.physics_Main.merge.AOD.f877_m1885._lb0674._0004.1"  --evtMax=10 trigAthAnalysis/trigAthAnalysisAlgJobOptions.py 2>&1 | tee zlog.txt
```
Or, modify ../source/trigMETAthenaAnalysisBasic/share/trigAthAnalysisAlgJobOptions.py to change number of events to process, input data file, etc, then
```
athena trigAthAnalysis/trigAthAnalysisAlgJobOptions.py 2>&1 | tee zlog.txt
```
If you just want to change number of events for a test run
```
athena  --evtMax=10 trigAthAnalysis/trigAthAnalysisAlgJobOptions.py 2>&1 | tee zlog.txt
```
This will produce "myfile.root".  


### Options


## Run on ESD
In the source/trigMETAthenaAnalysisBasic/share/trigAthAnalysisAlgJobOptions.py, change
```
jps.AthenaCommonFlags.AccessMode = "POOLAccess"
jps.AthenaCommonFlags.FilesInput = ["/hep300/data-shared/MET/data17_ESD/data17_13TeV.00338349.express_express.recon.ESD.f877/data17_13TeV.00338349.express_express.recon.ESD.f877._lb0160._SFO-ALL._0001.1"]
```
Then, just run it as usual.






# Next Time
## When you re-login
```
cd trigMETAthenaAnalysis/source
asetup
cd ../
source build/${AthAnalysis_PLATFORM}/setup.sh
cd run/
mkdir testRun02
cd testRun02/
athena  --evtMax=10 trigAthAnalysis/trigAthAnalysisAlgJobOptions.py 2>&1 | tee zlog.txt
```
This time the 'asetup' does not have to be issued under the source directory.  
Dont' forget to sourec the setup.sh.  

### acm version
```
cd build
acmSetup
```


## When you modify your code
```
cd build/
make
cd ../run/testRun02/
athena  --evtMax=10 trigAthAnalysis/trigAthAnalysisAlgJobOptions.py 2>&1 | tee zlog.txt
```



# Submit to Grid





# Tips
## Switches
There are some choices in the code. Those with current setting are:  
- m_doGRL = 1  
- m_doMuonTrigSelection = 0;

Followings are to avoid a crash with missing containers
- m_do2016 = 1  
This replace mht_em->mht, trkmht->tclcw, trkmht_FTK->tclcw  
- m_doClusters = 0  
Set 1 to include topocluster container
- m_doTracks = 0  
Set 1 to include track container
- m_doTrigTowers = 0  
Set 1 to include trigger tower container. You need to run on ESD to access the trigger tower container.


## When you include a new package
When you include a new package in the trigxAODAnalysis.h or trigxAODAnalysis.cxx, Add the package to trigMETAnalysisBasic/CMakeLists.txt
```
   LINK_LIBRARIES EventLoop xAODEventInfo AsgAnalysisInterfaces
   NewPackage
```

## Location of GRL
```
 https://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData/GoodRunsLists/
```

## Dealing with Branches
Create a new branch in your local repository
```
git checkout -b a_new_branch
```
Push the new branch to originnal repository
```
git push origin a_new_branch
```
See the current branch
```
git branch
```
Switch between branches
```
git branch branch_B
```
Check out a branch from original repository
```
git checkout -b your_branch_name origin/branch_to_checkout --no-track
git fetch origin
```


# Macros to produce histograms and plots
The macros and sample input files are in the 'macros' directory.  

## Tree to histograms
Use mkHist_trigTree.C  
Start up Root and 
```
[] .x mkHist_trigTree.C
```
This will produe histograms including efficiency histograms.


## Histograms to plots
mkPlot_allHistos.C : plot all hitograms in the input file.  
mkPlot_overlayHistos_oneInput.C : make an overlay plot from selected histograms in one input file.  
mkPlot_overlayHistos_multiInput.C : make overlay plots for selected histograms from multiple input files.  
Usage is same for these macros
```
[] .x mkPlot_allHistos.C
```
