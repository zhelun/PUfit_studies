#include <iostream>
#include <fstream>

//gROOT->Reset();


void mkPlot_allHistos() {
  // root macro to make plots for all histogram in a input root file
  // Usage:
  //   [].x mkPlot_allHistos.C
  //

  // ### SetUp (Modify these values) ###
  // expected number of histograms
  const int nHistos = 100;
  // input files
  TString inDir = "."; //directry for input files
  TString inFileName = "hist_trigMET.root"; //input file name
  TString inFile = inDir+"/"+inFileName;
  // output files. Don't forget to create outDir before run this macro
  TString outDir = "plots"; //directroy for output files
  TString outFileNamePrefix = "plot"; //common prefix for output file names
  TString outFileFormat = "pdf";  //pdf, eps, png
  Int_t showStat = 1; //0=no stat bos, 1=show stat box
  Int_t showTitle = 1;  //0=no plot title, 1=showplot  title
  Int_t useLogY = 0; //0=linear, 1=LogY
  // ### End of SetUp ###

  // open inout file and read in histograms
  TH1F *h[nHistos];
  TString outFileName[nHistos];
  TString outFile[nHistos];
  cout << "opening : " << inFile << endl;
  TFile *f = new TFile(inFile);
  //f->GetListOfKeys()->Print();
  TIter nexth(f->GetListOfKeys());   
  TKey* keyh;   
  Int_t ihist=0;
  Int_t ihistSig = 0;
  while ((keyh=(TKey*)nexth())) { 
    // get key (histo) name
    TString histName = keyh->GetName();       
    //std::cout << "key name = " << histName << std::endl; 
    // select histograms
    if (histName.BeginsWith("h_")) {
      // get the histo
      h[ihist] = (TH1F*)f->Get(histName);
      // set output file name
      outFileName[ihist] = outFileNamePrefix+"_"+histName+"."+outFileFormat;
      outFile[ihist] = outDir+"/"+outFileName[ihist];
      //std::cout << "outFileName["  << ihist << "] = " << outFileName[ihist] << std::endl; 
      ihist++;
      if (ihist == nHistos) {
	std::cout << "### Number of input histograms are too many. Increase nHistos (currently " << nHistos << ") ###" << std::endl;
	return;
      }
    }
  }

  // Plot histograms and save the plots.
  if (!showStat) gStyle->SetOptStat(kFALSE);
  if (!showTitle) gStyle->SetOptTitle(kFALSE);

  // Open canvas and plot
  TCanvas * c[nHistos];
  for (int i=0; i<ihist; i++) {
    c[i] = new TCanvas("c_"+outFileName[i],"Plots",0,0,800,800);
    if(useLogY) gPad->SetLogy();
    // setup axis etc.
    //h[i]->SetTitle(0);
    //h[i]->GetYaxis()->SetTitle(yAxTitle[i]);
    //h[i]->GetYaxis()->CenterTitle();
    h[i]->GetYaxis()->SetTitleOffset(1.0);
    h[i]->GetYaxis()->SetTitleFont(63);
    h[i]->GetYaxis()->SetTitleSize(20);
    //h[i]->GetYaxis()->SetNdivisions(504);
    h[i]->GetYaxis()->SetLabelFont(63);
    h[i]->GetYaxis()->SetLabelSize(20);
    // h[i]->GetXaxis()->SetTitle(xAxTitle[i]);
    //h[i]->GetXaxis()->CenterTitle();
    h[i]->GetXaxis()->SetTitleOffset(1.0);
    h[i]->GetXaxis()->SetTitleFont(63);
    h[i]->GetXaxis()->SetTitleSize(20);
    //h[i]->GetXaxis()->SetNdivisions(505);
    h[i]->GetXaxis()->SetLabelFont(63);
    h[i]->GetXaxis()->SetLabelSize(20);
    //h[i]->SetLineColor(1);
    // draw and save 
    h[i]->Draw();
    c[i]->Print(outFile[i]);
  }


}
