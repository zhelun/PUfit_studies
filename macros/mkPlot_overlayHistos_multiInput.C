#include <iostream>
#include <fstream>

//gROOT->Reset();


void mkPlot_overlayHistos_multiInput() {
  // root macro to overlay histograms in multiple input files.
  // Usage:
  //   [].x mkPlo_overlayHistos_multiInput.C
  //

  // ### SetUp (Modify these values) ###
  // input files
  TString inDir = "."; //directry for input files
  enum {nFiles = 2};
  TString inFileName[nFiles];
  TString inFile[nFiles];
  inFileName[0] = "hist-data17_13TeV.00339590.physics_Main.merge.AOD.f889_m1831_r9949_r10241_r10242_p3313.root";
  inFileName[1] = "hist-data17_13TeV.00338349.physics_Main.merge.AOD.f877_m1885.root";
  for (int j=0; j<nFiles; j++) {
    inFile[j] = inDir+"/"+inFileName[j];
  }
  TString inFileInfo[nFiles];
  inFileInfo[0] = "Run 339590";
  inFileInfo[1] = "Run 338349";

  // output files. Don't forget to create outDir before run this macro
  TString outDir = "plots"; //directroy for output files
  TString outFileNamePrefix = "plot"; //common prefix for output file names
  TString outFileNameBase = "compareMET";
  TString outFileFormat = "pdf";  //pdf, eps, png
  // plot format
  Int_t showStat = 0; //0=no stat bos, 1=show stat box
  Int_t showTitle = 1;  //0=no plot title, 1=showplot  title
  Int_t useLogY = 1; //0=linear, 1=LogY
  //##############

  // Histogram names
  enum {nHist = 5};

  TString histName[nHist];
  histName[0] = "h_MET_L1";
  histName[1] = "h_MET_cell";
  histName[2] = "h_MET_mht";
  histName[3] = "h_MET_topocl";
  histName[4] = "h_MET_topocl_PUC";
  //histName[5] = "h_MET_topocl_PS";
  //histName[6] = "h_MET_tcem";
  //histName[7] = "h_MET_FEB";
  //for (int i=0; i<nHist; i++) {cout << histName[i] << endl;}  

  // x-axis title
  TString xAxTitle[nHist];
  xAxTitle[0] = TString("MET (GeV)");
  xAxTitle[1] = TString("MET (GeV)");
  xAxTitle[2] = TString("MET (GeV)");
  xAxTitle[3] = TString("MET (GeV)");
  xAxTitle[4] = TString("MET (GeV)");
  //xAxTitle[5] = TString("MET (GeV)");
  //xAxTitle[6] = TString("MET (GeV)");
  //xAxTitle[7] = TString("MET (GeV)");
  // y-axis title
  TString yAxTitle[nHist];
  yAxTitle[0] = TString("Number of events");
  yAxTitle[1] = TString("Number of events");
  yAxTitle[2] = TString("Number of events");
  yAxTitle[3] = TString("Number of events");
  yAxTitle[4] = TString("Number of events");
  //yAxTitle[5] = TString("Number of events");
  //yAxTitle[6] = TString("Number of events");
  //yAxTitle[7] = TString("Number of events");
  // ### End of SetUp ###


  // Open input hist file and get histograms
  TFile *f[nFiles];
  for (int j=0; j<nFiles; j++) {
    cout << "opening : " << inFile[j] << endl;
    f[j] = new TFile(inFile[j]);
    //f[j]->ls();
  }
  
  TString histName2[nHist];
  for (int i=0; i<nHist; i++) {
    histName2[i]=histName[i]+";1";
  }
  
  TH1F * inHist[nFiles][nHist];
  for (int j=0; j<nFiles; j++) {
    for (int i=0; i<nHist; i++) { 
      //cout << histName2[i] << endl;
      inHist[j][i] = (TH1F *)f[j]->Get(histName2[i]);
    }
  }
    

  // Set output file names
  TString outFile[nHist];
  for (int i=0; i<nHist; i++) {
    outFile[i] = outDir+"/"+outFileNamePrefix+"_"+outFileNameBase+"_"+histName[i]+"."+outFileFormat;
    //cout << outFile[i] << endl;
  }


  // Plot histograms and save the plots.
  if (!showStat) gStyle->SetOptStat(kFALSE);
  if (!showTitle) gStyle->SetOptTitle(kFALSE);

  // Open canvas and plot
  TCanvas * c[nHist];
  for (int i=0; i<nHist; i++) {
    c[i] = new TCanvas("c"+histName[i],"Plots",0,0,800,800);
    if(useLogY) gPad->SetLogy();
    // get max of histograms
    Double_t maxY = 0;
    for (int j=0; j<nFiles; j++) {
      if (inHist[j][i]->GetMaximum() > maxY)  maxY = inHist[j][i]->GetMaximum(); 
    }
    // set properties and draw
    for (int j=0; j<nFiles; j++) {
      inHist[j][i]->SetTitle(0);
      inHist[j][i]->SetMaximum(maxY*1.2);
      inHist[j][i]->GetYaxis()->SetTitle(yAxTitle[i]);
      //inHist[j][i]->GetYaxis()->CenterTitle();
      inHist[j][i]->GetYaxis()->SetTitleOffset(2.0);
      inHist[j][i]->GetYaxis()->SetTitleFont(63);
      inHist[j][i]->GetYaxis()->SetTitleSize(20);
      //inHist[j][i]->GetYaxis()->SetNdivisions(504);
      inHist[j][i]->GetYaxis()->SetLabelFont(63);
      inHist[j][i]->GetYaxis()->SetLabelSize(20);
      inHist[j][i]->GetXaxis()->SetTitle(xAxTitle[i]);
      //inHist[j][i]->GetXaxis()->CenterTitle();
      inHist[j][i]->GetXaxis()->SetTitleOffset(1.5);
      inHist[j][i]->GetXaxis()->SetTitleFont(63);
      inHist[j][i]->GetXaxis()->SetTitleSize(20);
      //inHist[j][i]->GetXaxis()->SetNdivisions(505);
      inHist[j][i]->GetXaxis()->SetLabelFont(63);
      inHist[j][i]->GetXaxis()->SetLabelSize(20);
      inHist[j][i]->SetLineColor(j+1);
      
      inHist[j][i]->Draw("same");
    }

    TLegend * leg = new TLegend(0.51,0.71,0.89,0.89);
    leg->SetTextSize(.023);
    leg->SetFillColor(0);
    TLegendEntry *pley;
    for (int j=0; j<nFiles; j++) {
      pley = leg->AddEntry(inHist[j][i],inFileInfo[j],"L");
    }
    leg->Draw();
    
    c[i]->Print(outFile[i]);
  }

}
