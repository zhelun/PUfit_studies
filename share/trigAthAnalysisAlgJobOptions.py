#Skeleton joboption for a simple analysis job

#---- Minimal job options -----

jps.AthenaCommonFlags.AccessMode = "ClassAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name

jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]  #register output files like this. MYSTREAM is used in the code

athAlgSeq += CfgMgr.trigAthAnalysisAlg()                               #adds an instance of your alg to the main alg sequence

#from InDetTrackSelectionTool.InDetTrackSelectionToolConf import InDet__ToolTester
#alg = InDet__ToolTester()

#alg.TrackSelectionTool.CutLevel = "TightPrimary"

#athAlgSeq+=alg




#---- Options you could specify on command line -----
jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
#jps.AthenaCommonFlags.FilesInput = ["/hep300/data/khamano/data17/data17_13TeV.00338349.physics_Main.merge.AOD.f877_m1885/data17_13TeV.00338349.physics_Main.merge.AOD.f877_m1885._lb0674._0004.1"]        #set on command-line with: --filesInput=...


base_name="/hep300/data/zhelunli/data17_13TeV.00339435.physics_Main.merge.DAOD_ZMUMU.f904_m1831_f913_m1920/data17_13TeV.00339435.physics_Main.merge.DAOD_ZMUMU.f904_m1831_f913_m1920._"
files=[]
for i in range(925):
	num=i+1
	if num<10:
		current="000"+str(num)+".1"
	elif num<100:
		current="00"+str(num)+".1"
	else :
		current="0"+str(num)+".1"
	fullname=base_name+current
	files.append(fullname)



#base_name="/hep300/data-shared/MET/data17_13TeV_ZMUMU_DAOD_AND_DESDM/"
#files=[base_name+"DAOD_ZMUMU.13253524._000001.pool.root.1"]

#jps.AthenaCommonFlags.FilesInput = ["/hep300/data/zhelunli/data17_13TeV.00339435.physics_Main.merge.DAOD_ZMUMU.f904_m1831_f913_m1920/"]
jps.AthenaCommonFlags.FilesInput =files
include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above

