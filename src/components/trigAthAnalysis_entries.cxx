
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../trigAthAnalysisAlg.h"

DECLARE_ALGORITHM_FACTORY( trigAthAnalysisAlg )

DECLARE_FACTORY_ENTRIES( trigAthAnalysis ) 
{
  DECLARE_ALGORITHM( trigAthAnalysisAlg );
}
