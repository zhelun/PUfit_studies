// trigAthAnalysis includes
#include "trigAthAnalysisAlg.h"

#include "xAODEventInfo/EventInfo.h"
#include <xAODCore/AuxContainerBase.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODBase/IParticleHelpers.h>
#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTau/TauJetContainer.h>
#include <xAODMissingET/MissingETAuxContainer.h>
#include <xAODMissingET/MissingETAssociationMap.h>
#include <xAODMissingET/MissingETContainer.h>
#include "xAODTrigL1Calo/TriggerTowerContainer.h"
//#include "CaloEvent/CaloCellContainer.h"
//#include "CaloEvent/CaloCell.h"

#include <xAODTrigger/EnergySumRoI.h>
#include <xAODTrigMissingET/TrigMissingETContainer.h>
#include <xAODTrigMissingET/TrigMissingETAuxContainer.h>

#include <TSystem.h>
#include <math.h> 
#include <TFile.h>
#include <TMatrixD.h>



namespace {
  // Helper class
  struct Tower {
    float sumEt{0.};
    float px{0.};
    float py{0.};
    float pt() { return sqrt(px*px+py*py); }
    float cosPhi() { return pt() > 0 ? px/pt() : 1.; }
    float sinPhi() { return pt() > 0 ? py/pt() : 0.; }
    float phi{0.};
    float eta{0.};
    float hs_trk{0.};//sumPt of trk
    float trkx{0.};
    float trky{0.};
    float e_all(){return sumEt+hs_trk;}
    float area{0.};
    float variance{0.};
    Tower& operator+=(const xAOD::CaloCluster& clus) {
      float sinPhi;
      float cosPhi;
      sincosf(clus.phi(xAOD::CaloCluster::CALIBRATED), &sinPhi, &cosPhi);
      float Et = clus.pt(xAOD::CaloCluster::CALIBRATED);
      sumEt += Et;
      px += Et * cosPhi;
      py += Et * sinPhi;
      return *this;
    }
  };

 typedef ElementLink<xAOD::IParticleContainer> iplink_t;
 static const SG::AuxElement::ConstAccessor< std::vector<iplink_t > > dec_constitObjLinks("ConstitObjectLinks");


  double mean(std::vector<double> vec){
    double vec_size=vec.size()+0.01;
    double mean =0.0;
    for(int i =0; i<vec_size;i++){
	mean+=vec[i]/vec_size ;
	}

    return mean;
    }


  double int_mean(std::vector<int> vec){
    double vec_size=vec.size()+0.0000000001;
    double mean =0.0;

   
    for(int i =0; i<vec_size;i++){
	mean+=vec[i]/vec_size ;

	}
  
    return mean;
    }

  double max(std::vector<double> vec){
    double vec_size=vec.size()+0.01;
    double max =0.0;
    for(int i =0; i<vec_size;i++){
      if(vec[i]>max)max=vec[i];
	}

    return max;
    }

  
  double rms(std::vector<double> vec){
    int vec_size=vec.size();

    double mean =0.0;
    double rms=0.0;
    if (vec_size!=0){
    for(int i =0; i<vec_size;i++){
	mean+=vec[i]/vec_size ;
	}

  

   for (int j=0;j<vec_size;j++){
	rms+=(vec[j]-mean)*(vec[j]-mean)/vec_size ;
	}
   
   rms=sqrt(rms);
    }else {
    }
    return rms;
}



  double mag(double x,double y){
    return sqrt(x*x+y*y);  
  }


double feta(double eta,double g){
    double eta2=eta*eta;
    double eta4=eta2*eta2;
    double eta8=eta4*eta4;
    double a=-1.9616827617677786/100000.0;
    double b=0.05562904557049227;
    double c=-1.5363262367860067;
    double d=12.682644177471168;
    double f=a*eta8+b*eta4+c*eta2+d;
    f=g*f;

    f=g;
    return f;
 
}



double fveta(double eta,double g){
    double eta2=eta*eta;
    double eta4=eta2*eta2;
    double eta8=eta4*eta4;
    double a=-0.0003950483246864856;
    double b=0.5479644843432284;
    double c=-10.742669093550283;
    double d=12.695934963265797;
    double f=a*eta8+b*eta4+c*eta2+d;
    f=g*f;

    f=g;
    return f;
    
   }



}



trigAthAnalysisAlg::trigAthAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


trigAthAnalysisAlg::~trigAthAnalysisAlg() {}


StatusCode trigAthAnalysisAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  m_h_averageIPC = new TH1F("h_averageIPC","averageIPC",70,0,70);
  CHECK( histSvc()->regHist("/MYSTREAM/averageIPC", m_h_averageIPC) ); //registers histogram to output stream
  m_h_jetPt = new TH1F("h_jetPt","jetPt",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/jetPt", m_h_jetPt) ); //register
  m_h_elPt = new TH1F("h_elPt","elPt",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/elPt", m_h_elPt) ); //register
  m_h_photPt = new TH1F("h_photPt","photPt",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/photPt", m_h_photPt) ); //register
  m_h_muPt = new TH1F("h_muPt","muPt",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/muPt", m_h_muPt) ); //register
  m_h_tauPt = new TH1F("h_tauPt","tauPt",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/tauPt", m_h_tauPt) ); //register
  m_h_nClusters = new TH1F("h_nClusters","nClusters",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/nClusters", m_h_nClusters) ); //register
  m_h_clusterE = new TH1F("h_clusterE","clusterE",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/clusterE", m_h_clusterE) ); //register
  m_h_trackPt = new TH1F("h_trackPt","trackPt",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/trackPt", m_h_trackPt) ); //register
  m_h_cellE = new TH1F("h_cellE","cellE",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/cellE", m_h_cellE) ); //register
  m_h_cellEta = new TH1F("h_cellEta","cellEta",100,-5,5);
  CHECK( histSvc()->regHist("/MYSTREAM/cellEta", m_h_cellEta) ); //register
  m_h_cellPhi = new TH1F("h_cellPhi","cellPhi",100,-3.2,3.2);
  CHECK( histSvc()->regHist("/MYSTREAM/cellPhi", m_h_cellPhi) ); //register
  m_h_L1_Et = new TH1F("h_L1_Et","L1_Et",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/L1_Et", m_h_L1_Et) ); //register
  m_h_cell_Et = new TH1F("h_cell_Et","cell_Et",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/cell_Et", m_h_cell_Et) ); //register
  m_h_mht_Et = new TH1F("h_mht_Et","mht_Et",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/mht_Et", m_h_mht_Et) ); //register
  m_h_tclcw_Et = new TH1F("h_tclcw_Et","tclcw_Et",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/tclcw_Et", m_h_tclcw_Et) ); //register
  m_h_tclcw_pufit_Et = new TH1F("h_tclcw_pufit_Et","tclcw_pufit_Et",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/tclcw_pufit_Et", m_h_tclcw_pufit_Et) ); //register
  m_h_counts = new TH1F("h_counts","counts",12,0,12);
  CHECK( histSvc()->regHist("/MYSTREAM/counts", m_h_counts) ); //register
  m_h_MET_TST = new TH1F("h_MET_TST","MET_TST",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/MET_TST", m_h_MET_TST) ); //register
  m_h_MET_TST_noMu = new TH1F("h_MET_TST_noMu","MET_TST_noMu",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/MET_TST_noMu", m_h_MET_TST_noMu) ); //register
  m_h_MET_RefEle = new TH1F("h_MET_RefEle","MET_RefEle",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/MET_RefEle", m_h_MET_RefEle) ); //register
  m_h_MET_RefMuon = new TH1F("h_MET_RefMuon","MET_RefMuon",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/MET_RefMuon", m_h_MET_RefMuon) ); //register
  m_h_MET_RefJet = new TH1F("h_MET_RefJet","MET_RefJet",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/MET_RefJet", m_h_MET_RefJet) ); //register
  m_h_MET_TST_soft = new TH1F("h_MET_TST_soft","MET_TST_soft",100,0,200);
  CHECK( histSvc()->regHist("/MYSTREAM/MET_TST_soft", m_h_MET_TST_soft) ); //register

  m_h_nhard_track = new TH1F("h_nhard_track","number of hard tracks",100,0,3500);
  CHECK( histSvc()->regHist("/MYSTREAM/nhard_track", m_h_nhard_track) ); //register
 
  
  m_h_zmass = new TH1F("h_zmass","mass of the Z boson",100,60,120);
  CHECK( histSvc()->regHist("/MYSTREAM/zmass", m_h_zmass) ); //register
  


  // create a new TTree and connect it to that output
  m_tree = new TTree("trigTree","trigTree");
  CHECK( histSvc()->regTree("/MYSTREAM/trigTree", m_tree) ); //registers tree to output stream
  // define what braches will go in that tree
  m_tree->Branch ("runNumber", &m_RunNumber);
  m_tree->Branch ("eventNumber", &m_EventNumber);
  m_tree->Branch ("bcid", &m_BCID);
  m_tree->Branch ("lumiBlock", &m_LB);
  m_tree->Branch ("averageInteractionsPerCrossing", &m_mu);
  m_tree->Branch ("actualInteractionsPerCrossing", &m_muActual);
  m_tree->Branch ("nVtx", &m_nVtx);
  m_tree->Branch ("passMu", &m_passMu);
  m_tree->Branch ("passL1", &m_passL1);
  m_tree->Branch ("nJets", &m_nJets);
  m_tree->Branch ("jet1_Pt", &m_jet1_Pt);
  m_tree->Branch ("nElectrons", &m_nElectrons);
  m_tree->Branch ("el1_Pt", &m_el1_Pt);
  m_tree->Branch ("el1_Eta", &m_el1_Eta);
  m_tree->Branch ("el1_Phi", &m_el1_Phi);
  m_tree->Branch ("el1_M", &m_el1_M);
  m_tree->Branch ("el1_MediumID", &m_el1_MediumID);
  m_tree->Branch ("el1_GradientIso", &m_el1_GradientIso);
  m_tree->Branch ("el2_Pt", &m_el2_Pt);
  m_tree->Branch ("el2_Eta", &m_el2_Eta);
  m_tree->Branch ("el2_Phi", &m_el2_Phi);
  m_tree->Branch ("el2_M", &m_el2_M);
  m_tree->Branch ("el2_MediumID", &m_el2_MediumID);
  m_tree->Branch ("el2_GradientIso", &m_el2_GradientIso);
  m_tree->Branch ("nMuons", &m_nMuons);
  m_tree->Branch ("nhard_muons",&m_nhard_muons);
  m_tree->Branch ("mu1_Pt", &m_mu1_Pt);
  m_tree->Branch ("mu1_Eta", &m_mu1_Eta);
  m_tree->Branch ("mu1_Phi", &m_mu1_Phi);
  m_tree->Branch ("mu1_M", &m_mu1_M);
  m_tree->Branch ("mu1_MediumID", &m_mu1_MediumID);
  m_tree->Branch ("mu1_GradientIso", &m_mu1_GradientIso);
  m_tree->Branch ("mu1_Charge",&m_mu1_Charge);
  m_tree->Branch ("mu2_Pt", &m_mu2_Pt);
  m_tree->Branch ("mu2_Eta", &m_mu2_Eta);
  m_tree->Branch ("mu2_Phi", &m_mu2_Phi);
  m_tree->Branch ("mu2_M", &m_mu2_M);
  m_tree->Branch ("mu2_MediumID", &m_mu2_MediumID);
  m_tree->Branch ("mu2_GradientIso", &m_mu2_GradientIso);
  m_tree->Branch ("mu2_Charge",&m_mu2_Charge);
  m_tree->Branch ("mu3_Pt", &m_mu3_Pt);
  m_tree->Branch ("mu3_Eta", &m_mu3_Eta);
  m_tree->Branch ("mu3_Phi", &m_mu3_Phi);
  m_tree->Branch ("mu3_M", &m_mu3_M);
  m_tree->Branch ("mu3_MediumID", &m_mu3_MediumID);
  m_tree->Branch ("mu3_GradientIso", &m_mu3_GradientIso);
  m_tree->Branch ("MET_cellEmulate", &m_MET_cellEmulate);
  m_tree->Branch ("Ex_cellEmulate", &m_Ex_cellEmulate);
  m_tree->Branch ("Ey_cellEmulate", &m_Ey_cellEmulate);
  m_tree->Branch ("sumEt_cellEmulate", &m_sumEt_cellEmulate);
  m_tree->Branch ("MET_L1", &m_MET_L1);
  m_tree->Branch ("Ex_L1", &m_Ex_L1);
  m_tree->Branch ("Ey_L1", &m_Ey_L1);
  m_tree->Branch ("MET_cell", &m_MET_cell);
  m_tree->Branch ("phi_cell", &m_phi_cell);
  m_tree->Branch ("Ex_cell", &m_Ex_cell);
  m_tree->Branch ("Ey_cell", &m_Ey_cell);
  m_tree->Branch ("Ez_cell", &m_Ez_cell);
  m_tree->Branch ("sumEt_cell", &m_sumEt_cell);
  m_tree->Branch ("MET_mht", &m_MET_mht);
  m_tree->Branch ("phi_mht", &m_phi_mht);
  m_tree->Branch ("Ex_mht", &m_Ex_mht);
  m_tree->Branch ("Ey_mht", &m_Ey_mht);
  m_tree->Branch ("Ez_mht", &m_Ez_mht);
  m_tree->Branch ("sumEt_mht", &m_sumEt_mht);
  m_tree->Branch ("MET_pufit", &m_MET_pufit);
  m_tree->Branch ("phi_pufit", &m_phi_pufit);
  m_tree->Branch ("Ex_pufit", &m_Ex_pufit);
  m_tree->Branch ("Ey_pufit", &m_Ey_pufit);
  m_tree->Branch ("Ez_pufit", &m_Ez_pufit);
  m_tree->Branch ("sumEt_pufit", &m_sumEt_pufit);
  m_tree->Branch ("MET_offline", &m_MET_offline);
  m_tree->Branch ("phi_offline", &m_phi_offline);
  m_tree->Branch ("Ex_offline", &m_Ex_offline);
  m_tree->Branch ("Ey_offline", &m_Ey_offline);
  m_tree->Branch ("MET_offlineNoMu", &m_MET_offlineNoMu);
  m_tree->Branch ("Ex_offlineNoMu", &m_Ex_offlineNoMu);
  m_tree->Branch ("Ey_offlineNoMu", &m_Ey_offlineNoMu);

  
  m_tree->Branch("offline_El_ex",&m_exEl);
  m_tree->Branch("offline_El_ey",&m_eyEl);
  m_tree->Branch("offline_El_met",&m_El_met);
  m_tree->Branch("offline_Mu_ex",&m_exMu);
  m_tree->Branch("offline_Mu_ey",&m_eyMu);
  m_tree->Branch("offline_Mu_met",&m_Mu_met);
  m_tree->Branch("offline_Jet_ex",&m_exJet);
  m_tree->Branch("offline_Jet_ey",&m_eyJet);
  m_tree->Branch("offline_Jet_met",&m_Jet_met);
  m_tree->Branch("offline_softex_TST",&m_softex_TST);
  m_tree->Branch("offline_softey_TST",&m_softey_TST);
  m_tree->Branch("offline_softmet_TST",&m_softmet_TST);
  m_tree->Branch("offline_softex_CST",&m_softex_CST);
  m_tree->Branch("offline_softey_CST",&m_softey_CST);

  m_tree->Branch("raw_jetx",&m_raw_jetx);
  m_tree->Branch("raw_jety",&m_raw_jety);
  m_tree->Branch("raw_photonx",&m_raw_photonx);
  m_tree->Branch("raw_photony",&m_raw_photony);
  m_tree->Branch("raw_elx",&m_raw_elx);
  m_tree->Branch("raw_ely",&m_raw_ely);
  m_tree->Branch("raw_taux",&m_raw_taux);
  m_tree->Branch("raw_tauy",&m_raw_tauy);
  m_tree->Branch("raw_muonx",&m_raw_muonx);
  m_tree->Branch("raw_muony",&m_raw_muony);
  m_tree->Branch("raw_tstx",&m_raw_tstx);
  m_tree->Branch("raw_tsty",&m_raw_tsty);


  m_tree->Branch("PATx",&m_PATx);
  m_tree->Branch("PATy",&m_PATy);
  m_tree->Branch("STx",&m_STx);
  m_tree->Branch("STy",&m_STy);
  
  m_tree->Branch("zmumu",&m_zmumu);
  m_tree->Branch("nhardjets",&m_nhardjets);
  m_tree->Branch("nhightowers",&m_nhightowers);
  m_tree->Branch("omega",&m_omega);
  m_tree->Branch("Ex0",&m_Ex0);
  m_tree->Branch("Ey0",&m_Ey0);
  m_tree->Branch("Px0",&m_Px0);
  m_tree->Branch("Py0",&m_Py0);
  m_tree->Branch("jet_phi",&m_jetphi);
  m_tree->Branch("lowtower_phi",&m_lowtower_phi);
  m_tree->Branch("lowtower_e",&m_lowtower_e);
  m_tree->Branch("lowtower_p",&m_lowtower_p);

  // GRL
  //const char* GRLFilePath = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20171130/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml";
  //const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
  //std::string fullGRLFilePath = PathResolver::FindCalibFile("GoodRunsLists/data17_13TeV/20171130/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml");
  std::string fullGRLFilePath = PathResolver::FindCalibFile("GoodRunsLists/data17_13TeV/20180309/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml");
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(fullGRLFilePath);
  CHECK(m_grl.setProperty("GoodRunsListVec", vecStringGRL));
  CHECK(m_grl.setProperty("PassThrough", false)); // true (default) will ignore GRL and pass all events
  //CHECK(m_grl.setProperty("PassThrough", true)); // just pass all events
  CHECK(m_grl.initialize());






  // jet rools
  // Jet cleaning tool

  m_scale="PF"; // This controls the scale used in the jets and clusters, EM or LC

// jet rools
  // Jet cleaning tool
  CHECK (m_jetCleaning.setProperty("CutLevel", "LooseBad"));
  CHECK (m_jetCleaning.setProperty("DoUgly", false));
  CHECK (m_jetCleaning.initialize());

  // Jet calibration tool
  CHECK( ASG_MAKE_ANA_TOOL(m_jetCalibTool, JetCalibrationTool) );
  if(m_scale=="EM"){
    CHECK( m_jetCalibTool.setProperty("JetCollection", "AntiKt4EMTopo") );
    //CHECK( m_jetCalibTool.setProperty("ConfigFile", "JES_MC16Recommendation_June2017.config") );
    //CHECK( m_jetCalibTool.setProperty("CalibSequence", "JetArea_Residual_EtaJES") );
    //CHECK( m_jetCalibTool.setProperty("IsData", false) );
    CHECK( m_jetCalibTool.setProperty("ConfigFile", "JES_data2016_data2015_Recommendation_Dec2016_rel21.config") );
    CHECK( m_jetCalibTool.setProperty("CalibSequence", "JetArea_Residual_EtaJES_GSC_Insitu") );
    CHECK( m_jetCalibTool.setProperty("IsData", true) ); // data
  }else if(m_scale=="PF"){
    CHECK( m_jetCalibTool.setProperty("JetCollection", "AntiKt4EMPFlow") );
    CHECK( m_jetCalibTool.setProperty("ConfigFile", "JES_data2017_2016_2015_Recommendation_PFlow_Aug2018_rel21.config") );
    CHECK( m_jetCalibTool.setProperty("CalibSequence", "JetArea_Residual_EtaJES_GSC_Insitu") );
    CHECK( m_jetCalibTool.setProperty("IsData", true) ); // data
  }
 
  CHECK( m_jetCalibTool.initialize() );

  // egamma tools
  // electron ID tool
  CHECK( m_electronSelectionTool.setProperty("primaryVertexContainer", "PrimaryVertices") );
  std::string confDir = "ElectronPhotonSelectorTools/offline/mc16_20170828/";
  //CHECK( m_electronSelectionTool.setProperty("ConfigFile", confDir + "ElectronLikelihoodLooseOfflineConfig2017_Smooth.conf") );
  CHECK( m_electronSelectionTool.setProperty("ConfigFile", confDir + "ElectronLikelihoodMediumOfflineConfig2017_Smooth.conf") );
  CHECK( m_electronSelectionTool.initialize() ); 

  // muon tools
  // muon calibration and smearing tool
  CHECK (m_muonCalibrationAndSmearingTool.initialize());
  // muon ID tool
  CHECK( m_muonSelectionTool.setProperty("MaxEta", 2.5) ); // default = 2.7
  CHECK( m_muonSelectionTool.setProperty("MuQuality", 1) ); // 0, 1, 2, 3 = Tight, Medium, Loose, VeryLoose
  CHECK( m_muonSelectionTool.initialize() );

  // isolation tool
  CHECK(m_isoSelectionTool.setProperty("ElectronWP","Gradient") ); 
  CHECK(m_isoSelectionTool.setProperty("MuonWP","Gradient") ); 
  CHECK(m_isoSelectionTool.initialize() ); 

  // Trigger tools
  CHECK (m_trigConfigTool.initialize());
  CHECK (m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle())); // connect the TrigDecisionTool to the ConfigTool
  CHECK (m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision"));
  CHECK (m_trigDecisionTool.initialize());

  // MET tools
  CHECK( m_metMaker.setProperty("JetSelection", "Tight") );
  //CHECK( m_metMaker.setProperty("JetJvtMomentName", "JvtRecal") );
  //CHECK( m_metMaker.setProperty("DoRemoveMuonJets", true ) );
  //CHECK( m_metMaker.setProperty("DoSetMuonJetEMScale", true ) );
   if(m_scale=="PF"){
    CHECK( m_metMaker.setProperty("DoPFlow", true) );
   }else{
        CHECK( m_metMaker.setProperty("DoPFlow", false) );
   }
  //CHECK( m_metMaker.setProperty("OutputLevel", MSG::INFO ) ); //VERBOSE, DEBUG, ERROR, WARNING, INFO

  CHECK( m_metMaker.retrieve() );



  //TRACKS SELECTION TOOL

 
   CHECK(m_trackSelectionTool.setProperty("CutLevel", "TightPrimary") );
   CHECK(m_trackSelectionTool.setProperty("maxZ0SinTheta", 3) );//1.5
   CHECK(m_trackSelectionTool.setProperty("maxD0", 2) );//1.5
   CHECK(m_trackSelectionTool.initialize() );


  // pufit parameters
  m_targetTowerWidth = 0.7; 
  m_maxEta = 5.;
  m_caloResSqrtTerm = 15.81; //tower variance r
  m_caloResFloor = 50; //tower variance floor r0
  m_nSigma = 5.0; //threshld
  m_constraintWeight = 1.; //relative weight s
  m_trimFactor = 0.9;

  m_nPhiBins = TMath::TwoPi() / m_targetTowerWidth;
  m_nEtaBins = 2 * m_maxEta / m_targetTowerWidth;
  m_nTowers = m_nPhiBins * m_nEtaBins;
  m_towerEtaWidth = 2 * m_maxEta / m_nEtaBins;
  m_towerPhiWidth = TMath::TwoPi() / m_nPhiBins;

  m_nEvents = 0;
  m_nEventsGRL = 0;
  m_nEventsMu = 0;
  m_nEventsL1 = 0;

  m_doGRL = 1; //1=GRL selection is on
  m_doMuTrigSelection = 0; //1=muon trigger selection
  m_trigMu1 = "HLT_mu26_ivarmedium";
  m_trigMu2 = "HLT_mu50";
  m_trigMu3 = "HLT_mu60_0eta105_msonly";
  m_trigMu4 = "HLT_2mu14";
  m_doL1Selection = 0; //1=L1_XE trigger selection
  m_trigL1pre = "L1_XE50";



  m_do2016 = 1;  //1=2016 HLT MET containers
  m_doClusters = 0;  //1=include cluter container
  m_doTracks = 0;  //1=include track container
  m_doCells = 0; //1=include cell container
  m_doTrigTowers = 0;

  
  return StatusCode::SUCCESS;
}

StatusCode trigAthAnalysisAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");


  return StatusCode::SUCCESS;
}

StatusCode trigAthAnalysisAlg::execute() {  


  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed


  if (m_nEvents%10000 == 0) {
    ATH_MSG_INFO (" " << m_nEvents << " events processed");
  }

  m_nEvents++;
  ATH_MSG_DEBUG ("Event # " << m_nEvents);

  //------------------------
  // Retrieve Containers
  //------------------------

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  ATH_MSG_DEBUG ("execute(): runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

  // get vertx container of interest
  const xAOD::VertexContainer* pVtx = nullptr;
  CHECK(evtStore()->retrieve(pVtx, "PrimaryVertices"));
  ATH_MSG_DEBUG ("execute(): number of primary vertices = " << pVtx->size());

  // get jet container of interest
  const xAOD::JetContainer* jets = nullptr;
  if(m_scale=="EM"){
 
  CHECK (evtStore()->retrieve( jets, "AntiKt4EMTopoJets"));
  ATH_MSG_DEBUG ("execute(): number of jets = " << jets->size());
  }else if(m_scale=="LC"){
  
  CHECK (evtStore()->retrieve( jets, "AntiKt4LCTopoJets"));
  ATH_MSG_DEBUG ("execute(): number of jets = " << jets->size());
  }else if(m_scale=="PF"){
   
  CHECK (evtStore()->retrieve( jets, "AntiKt4EMPFlowJets"));
  ATH_MSG_DEBUG ("execute(): number of jets = " << jets->size());

  }
 


  // get electron container of interest
  const xAOD::ElectronContainer* electrons = nullptr;
  CHECK (evtStore()->retrieve (electrons, "Electrons"));

  // get photon container of interest
  const xAOD::PhotonContainer* photons = nullptr;
    CHECK (evtStore()->retrieve (photons, "Photons"));

  // get muon container of interest
  const xAOD::MuonContainer* muons = nullptr;
  CHECK (evtStore()->retrieve (muons, "Muons"));

  // get tau container of interest
  const xAOD::TauJetContainer* taus = nullptr;
  CHECK (evtStore()->retrieve (taus, "TauJets"));

  // get reference MET
  const xAOD::MissingETContainer* refmet = nullptr;
  if (m_doClusters) {
    CHECK(evtStore()->retrieve(refmet,"MET_Reference_AntiKt4LCTopo"));
  }

  // get caloclusters; retrived EM scale, calibrate to elevate to LC scale
  const xAOD::CaloClusterContainer* topoclus = nullptr;
  // if (m_doClusters) {
   CHECK(evtStore()->retrieve(topoclus,"CaloCalTopoClusters"));
    //  }

  // get ID tracks
  const xAOD::TrackParticleContainer* tracks = nullptr;
  // if (m_doTracks) {
    CHECK(evtStore()->retrieve(tracks,"InDetTrackParticles"));
    // }

  // get L1 trigger towers
  const xAOD::TriggerTowerContainer *TTs = nullptr;
  if (m_doTrigTowers) {
    CHECK (evtStore()->retrieve(TTs,"xAODTriggerTowers"));
  }



  // initialize branch values for tree;
  m_RunNumber = -1;
  m_EventNumber = -1;
  m_BCID = -1;
  m_LB = -1;
  m_mu = 0;
  m_muActual = 0;
  m_nVtx = 0;
  m_passMu = 0;
  m_passL1 = 0;
  m_nJets = 0;
  m_jet1_Pt = 0;
  m_nElectrons = 0;
  m_el1_Pt = 0;
  m_el1_Eta = 0;
  m_el1_Phi = 0;
  m_el1_M = 0;
  m_el1_MediumID = 0;
  m_el1_GradientIso = 0;
  m_el2_Pt = 0;
  m_el2_Eta = 0;
  m_el2_Phi = 0;
  m_el2_M = 0;
  m_el2_MediumID = 0;
  m_el2_GradientIso = 0;
  m_nMuons = 0;
  m_nhard_muons=0;
  m_mu1_Pt = 0;
  m_mu1_Eta = 0;
  m_mu1_Phi = 0;
  m_mu1_M = 0;
  m_mu1_MediumID = 0;
  m_mu1_GradientIso = 0;  
  m_mu1_Charge=0;
  m_mu2_Pt = 0;
  m_mu2_Eta = 0;
  m_mu2_Phi = 0;
  m_mu2_M = 0;
  m_mu2_MediumID = 0;
  m_mu2_GradientIso = 0;
  m_mu2_Charge=0;
  m_mu3_Pt = 0;
  m_mu3_Eta = 0;
  m_mu3_Phi = 0;
  m_mu3_M = 0;
  m_mu3_MediumID = 0;
  m_mu3_GradientIso = 0;
  m_mu3_Charge=0;
  m_MET_cellEmulate = -1;
  m_Ex_cellEmulate = 1e9;
  m_Ey_cellEmulate = 1e9;
  m_sumEt_cellEmulate = -1;
  m_MET_L1 = -1;
  m_Ex_L1 = 1e9;
  m_Ey_L1 = 1e9;
  m_MET_cell = -1;
  m_phi_cell = -1;
  m_Ex_cell = 1e9;
  m_Ey_cell = 1e9;
  m_Ez_cell = 1e9;
  m_sumEt_cell = -1;
  m_MET_mht = -1;
  m_phi_mht = -1;
  m_Ex_mht = 1e9;
  m_Ey_mht = 1e9;
  m_Ez_mht = 1e9;
  m_sumEt_mht = -1;
  m_MET_pufit = -1;
  m_phi_pufit = -1;
  m_Ex_pufit = 1e9;
  m_Ey_pufit = 1e9;
  m_Ez_pufit = 1e9;
  m_sumEt_pufit = -1;
  m_MET_offline = -1;
  m_phi_offline = -1;
  m_Ex_offline = 1e9;
  m_Ey_offline = 1e9;
  m_MET_offlineNoMu = -1;
  m_Ex_offlineNoMu = 1e9;
  m_Ey_offlineNoMu = 1e9;
 
  m_exEl=1e9;
  m_eyEl=1e9;
  m_El_met=-1;
  m_exMu=1e9;
  m_eyMu=1e9;
  m_Mu_met=-1;
  m_exJet=1e9;
  m_eyJet=1e9;
  m_Jet_met=-1;
  m_softmet_TST=-1;
  m_softex_TST=1e9;
  m_softey_TST=1e9;
  m_softex_CST=1e9;
  m_softey_CST=1e9;
 
  m_raw_jetx=0.0;
  m_raw_jety=0.0;
  m_raw_elx=0.0;
  m_raw_ely=0.0;
  m_raw_photonx=0.0;
  m_raw_photony=0.0;
  m_raw_muonx=0.0;
  m_raw_muony=0.0;
  m_raw_taux=0.0;
  m_raw_tauy=0.0;
  m_raw_tstx=0.0;
  m_raw_tsty=0.0;


  m_PATx=0.0;
  m_PATy=0.0;
  m_STx=0.0;
  m_STy=0.0;

  m_zmumu=false;
  m_nhardjets=0;
  m_nhightowers=0;
  m_omega=0.0;

  m_Ex0=0.0;
  m_Ey0=0.0;
  m_Px0=0.0;
  m_Py0=0.0;
  std::vector<size_t> hardjet_index={};
  m_jetphi={};
  m_lowtower_phi={};
  m_lowtower_e={};
  m_lowtower_p={};





  //------------------------
  // Event Selection
  //------------------------

  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  bool isMC = false;
  // check if the event is MC
  if (eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION)) {
    isMC = true; // can do something with this later
  }

  // GRL selection
  if (!isMC) { // it's data!
    if (m_grl->passRunLB(*eventInfo)) {
      ATH_MSG_DEBUG ("keep event: GRL");
      m_nEventsGRL++;
    } else {
      ATH_MSG_DEBUG ("drop event: GRL");
      if (m_doGRL) return StatusCode::SUCCESS; // go to next event
    }
  } // end if not MC

  // muon trigger selection
  auto cgMu1 = m_trigDecisionTool->getChainGroup(m_trigMu1);
  auto cgMu2 = m_trigDecisionTool->getChainGroup(m_trigMu2);
  auto cgMu3 = m_trigDecisionTool->getChainGroup(m_trigMu3);
  auto cgMu4 = m_trigDecisionTool->getChainGroup(m_trigMu4);
  int passMuTrig = 0;
  if (cgMu1->isPassed()) passMuTrig = 1;
  if (cgMu2->isPassed()) passMuTrig = 1;
  if (cgMu3->isPassed()) passMuTrig = 1;
  if (cgMu4->isPassed()) passMuTrig = 1;
  if (passMuTrig > 0) {
    m_nEventsMu++;
  } else {
    ATH_MSG_DEBUG ("drop event: moun trigger");
    if (m_doMuTrigSelection) return StatusCode::SUCCESS;
  }
  // fill the branches of our trees
  m_passMu = passMuTrig;

  // L1_XE trigger selection
  auto cgL1pre = m_trigDecisionTool->getChainGroup(m_trigL1pre);
  int passL1Trig = 0;
  if (cgL1pre->isPassed()) {
    m_nEventsL1++;
    passL1Trig = 1;
  } else {
    ATH_MSG_DEBUG ("drop event: L1_XE trigger");
    if (m_doL1Selection) return StatusCode::SUCCESS;
  }
  // fill the branches of our trees
  m_passL1 = passL1Trig;


  //--------------------------------------------
  // Access Each Object and Fill Histograms
  //--------------------------------------------

  //------------
  // Event Info
  if(eventInfo) {
    m_RunNumber = eventInfo->runNumber();
    m_EventNumber = eventInfo->eventNumber();
    m_BCID = eventInfo->bcid();
    m_LB = eventInfo->lumiBlock();
    m_mu = eventInfo->averageInteractionsPerCrossing();
    m_muActual = eventInfo->actualInteractionsPerCrossing();
    m_h_averageIPC->Fill( eventInfo->averageInteractionsPerCrossing() ); //fill mu into histogram
  }


  //---------------------
  // Primary Vertices
  // fill the branches of our trees
  if (pVtx) {
    m_nVtx = pVtx->size();
  }


  //------------
  // Jsts
  // loop over the jets in the container and store pt
  unsigned numGoodJets = 0;
  double jet1Pt = 0;
 
    m_nJets = jets->size();
    for (const xAOD::Jet *jet : *jets) {
      if (!m_jetCleaning->keep (*jet)) continue; //only keep good clean jets
      ++ numGoodJets;
      double tmpPt = jet->pt() * 0.001; //GeV
      //double tmpEta = (jet)->eta();
      ATH_MSG_DEBUG ("execute(): original jet pt = " << tmpPt << " GeV");
      m_h_jetPt->Fill(tmpPt);
      if (tmpPt > jet1Pt) jet1Pt = tmpPt;
      // constituent of the jet
      //auto constituents = jet->getConstituents();
      //for (const auto& constit: constituents) {
      //  ATH_MSG_DEBUG ("execute(): jet constituent pt = " << (constit->pt() * 0.001) << " GeV");
      //  hist("h_jetConstPt")->Fill (constit->pt() * 0.001); // GeV
      //}
    } // end for loop over jets
  
  m_jet1_Pt = jet1Pt;
  ATH_MSG_DEBUG ("execute: number of good jets = " << numGoodJets);


  //------------
  // Electrons
  // loop over the electrons in the container
  double el1Pt = 0;
  double el1Eta = 0;
  double el1Phi = 0;
  double el1M = 0;
  int el1ID = 0;
  int el1Iso = 0;
  double el2Pt = 0;
  double el2Eta = 0;
  double el2Phi = 0;
  double el2M = 0;
  int el2ID = 0;
  int el2Iso = 0;
  if (electrons) {
    m_nElectrons = electrons->size();
    for (auto electron : *electrons) {
      double tmpPt = (electron)->pt() * 0.001; //GeV
      double tmpEta = (electron)->eta();
      double tmpPhi = (electron)->phi();
      double tmpM = (electron)->m() * 0.001; //GeV
      int tmpID = 0;
      int tmpIso = 0;
      if(m_electronSelectionTool->accept(electron)) {
        tmpID = 1;
      }
      if (m_isoSelectionTool->accept(*electron)) {
        tmpIso = 1;
      }
      ATH_MSG_DEBUG ("execute(): electron pt = " << tmpPt << " GeV"); 
      m_h_elPt->Fill (tmpPt);
      if (tmpPt > el1Pt) {
        el1Pt = tmpPt;
        el1Eta = tmpEta;
        el1Phi = tmpPhi;
        el1M = tmpM;
        el1ID = tmpID;
        el1Iso = tmpIso;
      } else if (tmpPt > el2Pt) {
        el2Pt = tmpPt;
        el2Eta = tmpEta;
        el2Phi = tmpPhi;
        el2M = tmpM;
        el2ID = tmpID;
        el2Iso = tmpIso;
      }
    } // end for loop over electrons
  }
  m_el1_Pt = el1Pt;
  m_el1_Eta = el1Eta;
  m_el1_Phi = el1Phi;
  m_el1_M = el1M;
  m_el1_MediumID = el1ID;
  m_el1_GradientIso = el1Iso;
  m_el2_Pt = el2Pt;
  m_el2_Eta = el2Eta;
  m_el2_Phi = el2Phi;
  m_el2_M = el2M;
  m_el2_MediumID = el2ID;
  m_el2_GradientIso = el2Iso;


  //------------
  // Photons
  // loop over the photons in the container
  if (photons) {
   for (auto photon : *photons) {
      ATH_MSG_DEBUG ("execute(): original photon pt = " << ((photon)->pt() * 0.001) << " GeV"); 
      m_h_photPt->Fill ((photon)->pt() * 0.001); // GeV
    } // end for loop over photons
  }

  //------------
  // Muons
  // loop over the muons in the container
 double mu1Pt = 0;
  double mu1Eta = 0;
  double mu1Phi = 0;
  double mu1M = 0;
  int mu1ID = 0;
  int mu1Iso = 0;
  int mu1Charge=0;
  int mu1IDcuts=0;
  double mu1D0sig=0;
  double mu1Zst=0;

  double mu2Pt = 0;
  double mu2Eta = 0;
  double mu2Phi = 0;
  double mu2M = 0;
  int mu2ID = 0;
  int mu2Iso = 0;
  int mu2Charge=0;
  int mu2IDcuts=0;
  double mu2D0sig=0;
  double mu2Zst=0;

  double mu3Pt = 0;
  double mu3Eta = 0;
  double mu3Phi = 0;
  double mu3M = 0;
  int mu3ID = 0;
  int mu3Iso = 0;
  int mu3IDcuts=0;
  double mu3D0sig=0;
  double mu3Zst=0;
  if (muons) {
    m_nMuons = muons->size();
    for (auto muon : *muons) {
      double tmpPt = (muon)->pt() * 0.001; //GeV
      double tmpEta = (muon)->eta();
      double tmpPhi = (muon)->phi();
      double tmpM = (muon)->m() * 0.001; //GeV
      int tmpID = 0;
      int tmpIso = 0;
      int tmpCharge=(muon)->charge();
      double tmpD0Sig = 0;
      double tmpZ0 = 0;
      double tmpZ0sinTheta = 0;
      int tmpIDCuts = 0;

      if(m_muonSelectionTool->accept(*muon))  tmpID = 1;
      
      if (m_isoSelectionTool->accept(*muon))  tmpIso = 1;
   
      if (m_muonSelectionTool->passedIDCuts(*muon)) tmpIDCuts = 1;

      //      tmpD0Sig = xAOD::TrackingHelpers::d0significance(muonTrack, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY());
      const xAOD::TrackParticle* muonTrack = muon->primaryTrackParticle();
      tmpZ0 = muonTrack->z0() + muonTrack->vz() - (*pVtx)[0]->z();
      tmpZ0sinTheta = tmpZ0*sin(muonTrack->theta());
    

      ATH_MSG_DEBUG ("execute(): original muon pt = " << tmpPt << " GeV"); 
      m_h_muPt->Fill (tmpPt); // GeV

     if (tmpPt > mu1Pt) {
       //setting original mu2 to be mu3
       mu3Pt = mu2Pt;
       mu3Eta = mu2Eta;
       mu3Phi = mu2Phi;
       mu3M = mu2M;
      
       mu3ID = mu2ID;
       mu3Iso = mu2Iso;
       mu3Zst=mu2Zst;
       mu3D0sig=mu2D0sig;
       mu3IDcuts=mu2IDcuts;

       //setting original mu1 to be mu2
       mu2Pt = mu1Pt;
       mu2Eta = mu1Eta;
       mu2Phi = mu1Phi;
       mu2M = mu1M;
       mu2Charge=mu1Charge;
       mu2ID = mu1ID;
       mu2Iso = mu1Iso;
      
       mu2Zst=mu1Zst;
       mu2D0sig=mu1D0sig;
       mu2IDcuts=mu1IDcuts;


        mu1Pt = tmpPt;
        mu1Eta = tmpEta;
        mu1Phi = tmpPhi;
        mu1M = tmpM;
        mu1ID = tmpID;
        mu1Iso = tmpIso;
        mu1Charge=tmpCharge;
	mu1Zst=tmpZ0sinTheta;
        mu1D0sig=tmpD0Sig;
        mu1IDcuts=tmpIDCuts;
       } else if (tmpPt > mu2Pt) {
       mu3Pt = mu2Pt;
       mu3Eta = mu2Eta;
       mu3Phi = mu2Phi;
       mu3M = mu2M;
       mu3ID = mu2ID;
       mu3Iso = mu2Iso;
       mu3Zst=mu2Zst;
       mu3D0sig=mu2D0sig;
       mu3IDcuts=mu2IDcuts;

    
        mu2Pt = tmpPt;
        mu2Eta = tmpEta;
        mu2Phi = tmpPhi;
        mu2M = tmpM;
        mu2ID = tmpID;
        mu2Iso = tmpIso;
	mu2Zst=tmpZ0sinTheta;
        mu2D0sig=tmpD0Sig;
        mu2IDcuts=tmpIDCuts;

      } else if (tmpPt > mu3Pt) {
        mu3Pt = tmpPt;
        mu3Eta = tmpEta;
        mu3Phi = tmpPhi;
        mu3M = tmpM;
        mu3ID = tmpID;
        mu3Iso = tmpIso;
	mu3Zst=tmpZ0sinTheta;
        mu3D0sig=tmpD0Sig;
        mu3IDcuts=tmpIDCuts;
      }
    } // end for loop over muons
  }
  m_mu1_Pt = mu1Pt;
  m_mu1_Eta = mu1Eta;
  m_mu1_Phi = mu1Phi;
  m_mu1_M = mu1M;
  m_mu1_MediumID = mu1ID;
  m_mu1_GradientIso = mu1Iso;
  m_mu1_Charge=mu1Charge;
  m_mu2_Pt = mu2Pt;
  m_mu2_Eta = mu2Eta;
  m_mu2_Phi = mu2Phi;
  m_mu2_M = mu2M;
  m_mu2_MediumID = mu2ID;
  m_mu2_GradientIso = mu2Iso;
  m_mu2_Charge=mu2Charge;
  m_mu3_Pt = mu3Pt;
  m_mu3_Eta = mu3Eta;
  m_mu3_Phi = mu3Phi;
  m_mu3_M = mu3M;
  m_mu3_MediumID = mu3ID;
  m_mu3_GradientIso = mu3Iso;
 

  //------------
  // Taus
  // loop over the taus in the container
  if (taus) {
    for (auto tau : *taus) {
      ATH_MSG_DEBUG ("execute(): original tau pt = " << ((tau)->pt() * 0.001) << " GeV"); 
      m_h_tauPt->Fill ((tau)->pt() * 0.001); // GeV
    } // end for loop over taus
  }


  //-------------
  // CaloClusters
  // loop ove clusters in the container
  if (m_doClusters && topoclus) {
    m_h_nClusters->Fill(topoclus->size());
    for (auto pclus : *topoclus ) {
      m_h_clusterE->Fill((pclus)->calE() * 0.001); //GeV
    } 
  }


  //--------------
  // ID tracks
  // loop over the tracks in the container
  if (m_doTracks && tracks) {
    for (auto track : *tracks) {
      m_h_trackPt->Fill((track)->pt() * 0.001); //GeV
    }
  }

  //--------------
  // Trigger Towers
  // loop over trigger towers
  if (m_doTrigTowers && TTs) {
    if (m_nEvents < 10) {
      ATH_MSG_INFO ("Event # " << m_nEvents);
      ATH_MSG_INFO (" Number of trigger towers = " << TTs->size());
      ATH_MSG_INFO (" Towers with jepET > 0:");
    }
    for (auto tt : *TTs) {
      float cpET = tt->cpET(); //GeV
      float jepET = tt->jepET();
      float eta = tt->eta();
      float phi = tt->phi();
      if (m_nEvents < 10 && jepET > 0) {
	ATH_MSG_INFO ("   cpET = " << cpET << ", jepET  = " << jepET << ", eta = " << eta << ", phi  = " << phi );
      }
    }
  }

  /*
  //--------------
  // Cells
  // loop over cells in the container
  if (m_doCells && cells) {
    for (auto cell : *cells) {
      m_h_cellE->Fill((cell)->energy() * 0.001); //GeV
      m_h_cellEta->Fill((cell)->eta());
      m_h_cellPhi->Fill((cell)->phi());
    }
  }
  */




  //###############################
  //###########calib jets##########
  //###############################
  
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets );
  CHECK(evtStore()->record( jets_shallowCopy.first,  "CalibJets"    ));
  CHECK(evtStore()->record( jets_shallowCopy.second, "CalibJetsAux."));
  xAOD::JetContainer* calibJets = jets_shallowCopy.first;
  xAOD::setOriginalObjectLink(*jets,*calibJets);
  //CHECK(evtStore()->record( calibJets,  "CaliblatedJets" ));
  for ( const auto& jet : *calibJets ) {
     ATH_MSG_DEBUG ("Jet pt before calibration = " << (jet->pt() * 0.001) << " GeV");
     if(!m_jetCalibTool->applyCalibration(*jet)) {
       //if(m_jetCalibTool->applyCorrection(*jet) == CP::CorrectionCode::Error) {
       ATH_MSG_DEBUG ("Jet calibration failed. Skipping this event.");
       return StatusCode::SUCCESS;
     }
     ATH_MSG_DEBUG ("Jet pt after calibration = " << (jet->pt() * 0.001) << " GeV");
  }











  //###############################
  //######## Offline MET ##########
  //###############################
  //
 

  // Calculate offline MET
  // coreMet term
  const xAOD::MissingETContainer *coreMet = nullptr;
  const xAOD::MissingETAssociationMap *metMap = nullptr;
  if(m_scale=="EM"){
    CHECK(evtStore()->retrieve( coreMet, "MET_Core_AntiKt4EMTopo" ));
    // MET association map
    CHECK(evtStore()->retrieve( metMap, "METAssoc_AntiKt4EMTopo" ));

  }else if(m_scale=="LC"){
    CHECK(evtStore()->retrieve( coreMet, "MET_Core_AntiKt4LCTopo" ));
    // MET association map
    CHECK(evtStore()->retrieve( metMap, "METAssoc_AntiKt4LCTopo" ));

  }else if(m_scale=="PF"){
    CHECK(evtStore()->retrieve( coreMet, "MET_Core_AntiKt4EMPFlow" ));
    // MET association map
    CHECK(evtStore()->retrieve( metMap, "METAssoc_AntiKt4EMPFlow" ));
  }
  // check primary vertex
  const xAOD::Vertex *vxt_obj = pVtx->at(0);
  if (pVtx && pVtx->size()>0 && vxt_obj->vertexType() == xAOD::VxType::PriVtx) {
    // Create a MissingETContainer with its aux store for each systematic
    xAOD::MissingETContainer*    newMetContainer    = new xAOD::MissingETContainer();
    xAOD::MissingETAuxContainer* newMetAuxContainer = new xAOD::MissingETAuxContainer();
    newMetContainer->setStore(newMetAuxContainer);
 

    // It is necessary to reset the selected objects before every MET calculation
    metMap->resetObjSelectionFlags();

    //Electrons
    ConstDataVector<xAOD::ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);
    for(const auto& el : *electrons) {
      if((el)->pt() < 25*1000) continue;
      if((el)->eta() < -2.47 || (el)->eta() > 2.47) continue;
      if(!m_electronSelectionTool->accept(el)) continue;
      if(!m_isoSelectionTool->accept(*el)) continue;
      metElectrons.push_back(el);
    }
    CHECK(m_metMaker->rebuildMET("RefEle",                   //name of metElectrons in metContainer
				     xAOD::Type::Electron,       //telling the rebuilder that this is electron met
				     newMetContainer,            //filling this met container
				     metElectrons.asDataVector(),//using these metElectrons that accepted our cuts
				     metMap));                   //and this association map
    
    //Muons
    ConstDataVector<xAOD::MuonContainer> metMuons(SG::VIEW_ELEMENTS);
    for(const auto& mu : *muons) {
      if((mu)->pt() < 25*1000) continue;
      if((mu)->eta() < -2.47 || (mu)->eta() > 2.47) continue;
      if(!m_muonSelectionTool->accept(*mu)) continue;
      if(!m_isoSelectionTool->accept(*mu)) continue;
      metMuons.push_back(mu);
    }
    CHECK(m_metMaker->rebuildMET("RefMuon",
				     xAOD::Type::Muon,
				     newMetContainer,
				     metMuons.asDataVector(),
				     metMap)); 

    //Now time to rebuild jetMet and get the soft term
    CHECK(m_metMaker->rebuildJetMET("RefJet",        //name of jet met
					"SoftClus",      //name of soft cluster term met
					"PVSoftTrk",     //name of soft track term met
					newMetContainer, //adding to this new met container
					calibJets,       //using this jet collection to calculate jet met
					coreMet,         //core met container
					metMap,          //with this association map
					true            //apply jet jvt cut
					));
  
    //Fiinally, sum up all the terms in the container to produce the "final" terms
    CHECK(m_metMaker->buildMETSum("FinalTrk",        //name of the total term to create (this is for TST)
				      newMetContainer,   //adding to this new met container
				      MissingETBase::Source::Track));
    CHECK(m_metMaker->buildMETSum("FinalClus",        //name of the total term to create (this is for CST)
				      newMetContainer,   //adding to this new met container
				      MissingETBase::Source::EMTopo));
    
    
    CHECK(evtStore()->record( newMetContainer,    "FinalMETContainer"));
    CHECK(evtStore()->record( newMetAuxContainer, "FinalMETAuxContainer"));


    // retreave met values from the new container
    const xAOD::MissingET *offlineMet_obj = 0;
    const xAOD::MissingET *offlineMetEl_obj = 0;
    //const xAOD::MissingET *offlineMetTau_obj = 0;
    const xAOD::MissingET *offlineMetMu_obj = 0;
    const xAOD::MissingET *offlineMetJet_obj = 0;
    const xAOD::MissingET *offlineMetSoft_obj = 0;
    const xAOD::MissingET *offlineCST_obj=0;
    if(newMetContainer->size()>0) {
      // offlinr TST met
      offlineMet_obj = (*newMetContainer)["FinalTrk"];     
      float ex = offlineMet_obj->mpx()/1000.0;
      ATH_MSG_DEBUG("  ex = " << ex << " GeV");
      float ey = offlineMet_obj->mpy()/1000.0;
      ATH_MSG_DEBUG("  ey = " << ey << " GeV");
      float met = offlineMet_obj->met()/1000.0;  //GeV
      ATH_MSG_DEBUG("  met = " << met << " GeV");
      float phi = offlineMet_obj->phi();
      ATH_MSG_DEBUG("  phi = " << phi << " GeV");
      m_h_MET_TST->Fill(met);
     
       //  CST term
      offlineCST_obj = (*newMetContainer)["SoftClus"];     
      float CSTex = offlineCST_obj->mpx()/1000.0;
     
      float CSTey = offlineCST_obj->mpy()/1000.0;
      float CSTmet = offlineCST_obj->met()/1000.0;  //GeV
      float CSTphi = offlineCST_obj->phi();
      
     




      // Electron term
      offlineMetEl_obj = (*newMetContainer)["RefEle"];
      float exEl = offlineMetEl_obj->mpx()/1000.0;
      float eyEl = offlineMetEl_obj->mpy()/1000.0;
      float metEl = offlineMetEl_obj->met()/1000.0;
      m_h_MET_RefEle->Fill(metEl);
      ATH_MSG_DEBUG ("execute(): RefEle = " << metEl << " = " << sqrt(exEl*exEl+eyEl*eyEl) << "?");
      
      // Muon term
      offlineMetMu_obj = (*newMetContainer)["RefMuon"];
      float exMu = offlineMetMu_obj->mpx()/1000.0;
      float eyMu = offlineMetMu_obj->mpy()/1000.0;
      float metMu = offlineMetMu_obj->met()/1000.0;
      m_h_MET_RefMuon->Fill(metMu);
      // subtract muon term
      float subEx = ex - exMu;
      float subEy = ey - eyMu;
      float subMet = sqrt(subEx*subEx+subEy*subEy);
      m_h_MET_TST_noMu->Fill(subMet);
      
      // Jet term
      offlineMetJet_obj = (*newMetContainer)["RefJet"];
      float exJet = offlineMetJet_obj->mpx()/1000.0;
      float eyJet = offlineMetJet_obj->mpy()/1000.0;
      float metJet = offlineMetJet_obj->met()/1000.0;
      m_h_MET_RefJet->Fill(metJet);
      ATH_MSG_DEBUG ("execute(): RefJet = " << metJet << " = " << sqrt(exJet*exJet+eyJet*eyJet) << "?");

      // soft term
      offlineMetSoft_obj = (*newMetContainer)["PVSoftTrk"];
      float softMet = offlineMetSoft_obj->met()/1000.0;
      float softMetx=offlineMetSoft_obj->mpx()/1000.0;
      float softMety=offlineMetSoft_obj->mpy()/1000.0;
      m_h_MET_TST_soft->Fill(softMet);

      // fill the branches of our trees
      m_MET_offline = offlineMet_obj->met()/1000.0;
      m_phi_offline = offlineMet_obj->phi();
      m_Ex_offline = offlineMet_obj->mpx()/1000.0;
      m_Ey_offline = offlineMet_obj->mpy()/1000.0;
      m_MET_offlineNoMu = subMet;
      m_Ex_offlineNoMu = subEx;
      m_Ey_offlineNoMu = subEy;

      m_softex_TST=softMetx;
      m_softey_TST=softMety;
      m_softex_CST=CSTex;
      m_softey_CST=CSTey;

      m_exEl=exEl;
      m_eyEl=eyEl;
      m_El_met=metEl;
      m_exMu=exMu;
      m_eyMu=eyMu;
      m_Mu_met=metMu;
      m_exJet=exJet;
      m_eyJet=eyJet;
      m_Jet_met=metJet;
 
      // std::cout<<CSTex<<std::endl;

      //retieving the index of hard jets in the Jet container 
      for(const auto& met : *newMetContainer) {   

	if(met->name()=="RefJet"){
	  
	for(const auto& cons : dec_constitObjLinks(*met)) {
	 const xAOD::IParticle* obj(*cons);
	 if(obj->type()==xAOD::Type::Jet) hardjet_index.push_back(obj->index());
       
	}//for loop ended

	}// if ref jet ended
	

      }// met container loop ended                                                                                           
   


    }

  }
  
  












  //###############################
  //########hard selection ########
  //###############################



       //muon

       double raw_muonx=0.0;
       double raw_muony=0.0;
       int nhard_mu=0;

       ConstDataVector<xAOD::MuonContainer> hard_mu(SG::VIEW_ELEMENTS);
       ConstDataVector<xAOD::MuonContainer> soft_mu(SG::VIEW_ELEMENTS);
       for (const auto *mu : *muons){
         double mu_eta=mu->eta();
	 double mu_pt=mu->pt()/1000.0;//in GeV
	 mu_eta=sqrt(mu_eta*mu_eta);
	 bool hard=false;
	 if((mu_pt>10)&&(mu_eta<2.5)) hard=true;
	 if(!m_muonSelectionTool->accept(*mu)) hard=false;
	 if(!m_isoSelectionTool->accept(*mu)) hard=false;

	 if(hard==true){
	   hard_mu.push_back(mu);
	   raw_muonx-=mu_pt*cos(mu->phi());
	   raw_muony-=mu_pt*sin(mu->phi());
	   nhard_mu++;
	 }else{
	   soft_mu.push_back(mu);
	 }
       
       } 

       m_raw_muonx=raw_muonx;
       m_raw_muony=raw_muony;
       m_nhard_muons=nhard_mu;

       //Jets
      
       ConstDataVector<xAOD::JetContainer> hardScatterJets(SG::VIEW_ELEMENTS);
       ConstDataVector<xAOD::JetContainer> pileupJets(SG::VIEW_ELEMENTS);
 
       double raw_jetx=0.0;
       double raw_jety=0.0;
  

       //loop around all jets in the container to compare the index with HS jets index
       for (const auto& jet : *calibJets){
	 auto current_index=jet->index();
	 bool hard_tag=false;
	 for (size_t i=0;i<hardjet_index.size();i++){
        
	   if(current_index==hardjet_index[i]){
	     hard_tag=true;
	     raw_jetx-=(jet->pt()/1000.0)*cos(jet->phi());
	     raw_jety-=(jet->pt()/1000.0)*sin(jet->phi());
	   }
	   
	 }// hard index loop
	 if (hard_tag==true){
	   hardScatterJets.push_back(jet);
	 }else{
	   pileupJets.push_back(jet);
	 }

       }//calib jets loop


       
       m_raw_jetx=raw_jetx;
       m_raw_jety=raw_jety;





       //electron

       double raw_elx=0.0;
       double raw_ely=0.0;
       
       ConstDataVector<xAOD::ElectronContainer> hard_el(SG::VIEW_ELEMENTS);
       ConstDataVector<xAOD::ElectronContainer> soft_el(SG::VIEW_ELEMENTS); 

       for (const auto *el : *electrons){
            
         double el_eta=el->eta();
	 double  el_eta_mag=sqrt(el_eta*el_eta);
	 double el_pt=el->pt()/1000.0;//in GeV
	 bool hard=false;
	 if((el_pt>10)&&((el_eta<1.37)||((el_eta_mag>1.52)&&(el_eta_mag<2.47)) )) hard=true;
	 if(!m_electronSelectionTool->accept(el)) hard=false;
	 if(!m_isoSelectionTool->accept(*el)) hard=false;
	 
	 if(hard==true){
	   hard_el.push_back(el);
	   raw_elx-=el_pt*cos(el->phi());
	   raw_ely-=el_pt*sin(el->phi());
	 }else{
	   soft_el.push_back(el);
	 }
	 

       } 
       m_raw_elx=raw_elx;
       m_raw_ely=raw_ely;

 

       //photon
       
       //quality requirements for photon are not yet implemented
       double raw_photonx=0.0;
       double raw_photony=0.0;

       double soft_photonx=0.0;
       double soft_photony=0.0;

       int nsoftphoton=0;

       ConstDataVector<xAOD::PhotonContainer> hard_photon(SG::VIEW_ELEMENTS);
       ConstDataVector<xAOD::PhotonContainer> soft_photon(SG::VIEW_ELEMENTS);
       for (const auto *photon : *photons){
         double pho_eta=photon->eta();
	 double pho_pt=photon->pt()/1000.0;//in GeV
	 pho_eta=sqrt(pho_eta*pho_eta);
	 bool hard=false;
	 if((pho_pt>25)&&((pho_eta<1.37)||((pho_eta>1.52)&&(pho_eta<2.47)) )) hard=true;

	 if(hard==true){
	   hard_photon.push_back(photon);
	   raw_photonx-=pho_pt*cos(photon->phi());
	   raw_photony-=pho_pt*sin(photon->phi());
	 }else{
	   soft_photon.push_back(photon);
	   soft_photonx-=pho_pt*cos(photon->phi());
	   soft_photony-=pho_pt*sin(photon->phi());
	   nsoftphoton++;
	 }
       
       } 
   
       //tau
       
       //quality requirements for tau photon are not yet implemented
       double raw_taux=0.0;
       double raw_tauy=0.0;

       ConstDataVector<xAOD::TauJetContainer> hard_tau(SG::VIEW_ELEMENTS);
       ConstDataVector<xAOD::TauJetContainer> soft_tau(SG::VIEW_ELEMENTS);
       for (const auto *tau : *taus){
	 double tau_eta=tau->eta();
	 tau_eta=sqrt(tau_eta*tau_eta);
	 double tau_pt=tau->pt()/1000.0;// in GeV
	 
	 bool hard= false;
	 if((tau_pt>25)&&((tau_eta<1.37)||((tau_eta>1.52)&&(tau_eta<2.47)) )) hard=true;
	  
	 if(hard==true){
	   raw_taux-=tau_pt*cos(tau->phi());
	   raw_tauy-=tau_pt*sin(tau->phi());
	   hard_tau.push_back(tau);
	 }else{
	   soft_tau.push_back(tau);
	 }
       
       } 

       m_raw_taux=raw_taux;
       m_raw_tauy=raw_tauy;





 


  //###############################
  //########Track Selection########
  //###############################

       double omega=1; // this is the factor for TST


       ConstDataVector<xAOD::TrackParticleContainer> pv_soft_tracks(SG::VIEW_ELEMENTS);

       //raw tst is computed and then compared to offline TST
       double raw_tstx=0.0;
       double raw_tsty=0.0;
       
  

       double current_pt=0.0;
       double current_phi=0.0;
       double current_eta=0.0;
       
  
       for (const xAOD::TrackParticle* track : *tracks){
	 current_pt=track->pt()/1000.0;//in GeV
	 current_phi=track->phi();
	 current_eta=track->eta();
	 
	   //first, only look at tracks accepted to be from HS vertex
	 if(m_trackSelectionTool->accept(*track,pVtx->at(0))){
	   
	   bool soft_track=true;
	   
	   // then check to see if the track is used in any HS objects:
	   for (const xAOD::Jet* jet : hardScatterJets){
	
	     std::vector<const xAOD::IParticle*> jettracks;
	     jet->getAssociatedObjects<xAOD::IParticle>(xAOD::JetAttribute::GhostTrack,jettracks);
	     
	     for(size_t iConst=0; iConst<jettracks.size(); ++iConst) {
	       
	       const xAOD::TrackParticle* pTrk = static_cast<const xAOD::TrackParticle*>(jettracks[iConst]);
	       if(pTrk->index()==track->index())soft_track=false;	
	      	
			 
	     }
            
	   }//jet finished



	   //checking electrons:
	   for (const auto el: hard_el){
	     double el_phi=el->phi();
	     double el_eta=el->eta();
	     double deltaPhi = fmod( TMath::TwoPi()+current_phi-el_phi , TMath::TwoPi() );
	     if (deltaPhi > M_PI) deltaPhi = TMath::TwoPi() - deltaPhi;
	     double deltaR = sqrt((current_eta-el_eta)*(current_eta-el_eta) + deltaPhi*deltaPhi);
	     if(deltaR<0.05) soft_track=false;	// 0.05 is from the offline paper		  
	     
	   }


	   //checking photons:
	   for (const auto photon: hard_photon){
	     double photon_phi=photon->phi();
	     double photon_eta=photon->eta();
	     double deltaPhi = fmod( TMath::TwoPi()+current_phi-photon_phi , TMath::TwoPi() );
	     if (deltaPhi > M_PI) deltaPhi = TMath::TwoPi() - deltaPhi;
	     double deltaR = sqrt((current_eta-photon_eta)*(current_eta-photon_eta) + deltaPhi*deltaPhi);
	     if(deltaR<0.05) soft_track=false;	// 0.05 is from the offline paper		  

	   }

	   //removing all muon associated:
	   auto current_index=track->index();
	   
	   for (const auto mu: hard_mu){
	     auto mu_id_index= mu->inDetTrackParticleLink().index();
	     if(mu_id_index==current_index){
	       soft_track=false;
	       
	     }
	   }
	 
	   if ((soft_track==true)){
	     raw_tstx-=omega*current_pt*cos(current_phi);
	     raw_tsty-=omega*current_pt*sin(current_phi);
	     
	     pv_soft_tracks.push_back(track);
				}		
	 }//pv tracks if finished

       }// all tracks loop finished
     
       //removing tst from putrack
     
       m_h_nhard_track->Fill(pv_soft_tracks.size());

       m_raw_tstx=raw_tstx;
       m_raw_tsty=raw_tsty;











  //###############################
  //########   nojet cluster    ##########
  //###############################

  ConstDataVector<xAOD::CaloClusterContainer> pileupClusters(SG::VIEW_ELEMENTS);
  ConstDataVector<xAOD::CaloClusterContainer> hardScatterClusters(SG::VIEW_ELEMENTS);

//dR selection method to associate clusters to jets:
  double EtaRange = 5.0;
 

  for (const auto *clus : *topoclus){
    //defining the pile up as true, set it to false later if it is in jets
    bool passedclus=true;
    
    double clus_eta;
    double clus_phi; 
    if(m_scale=="EM"){
      	clus_eta = clus->eta();
	clus_phi= clus->phi();
    }else{
	clus_eta = clus->eta(xAOD::CaloCluster::CALIBRATED);
	clus_phi= clus->phi(xAOD::CaloCluster::CALIBRATED);
    }
	if(-clus_eta > EtaRange || clus_eta > EtaRange) continue;

	// check for overlap with any hard jet

       for (const xAOD::Jet* jet : hardScatterJets){
 	 
          double jet_phi; double jet_eta;double jetRcut;
          jetRcut=jet->getSizeParameter();
	  jet_phi=jet->phi();
	  jet_eta=jet->eta();
	  double deltaPhi = fmod( TMath::TwoPi()+clus_phi-jet_phi , TMath::TwoPi() );
	  if (deltaPhi > M_PI) deltaPhi = TMath::TwoPi() - deltaPhi;
	  double deltaR = sqrt((clus_eta-jet_eta)*(clus_eta-jet_eta) + deltaPhi*deltaPhi);
	  passedclus = passedclus && deltaR > jetRcut;

	}

       if(passedclus==false){ 

	 hardScatterClusters.push_back(clus);
       }
	if (passedclus){
	 
	  pileupClusters.push_back(clus);
	}

  }
 






  // testing PU subtracted in jets and also calibrations:
  std::vector<double> jet_cor(hardScatterJets.size());
  std::vector<double> jet_phi;
  std::vector<double> jet_eta;
  std::vector<double> jet_area;
  size_t jet_index=0;
  if (hardScatterJets.size() > 0) {
    ATH_MSG_DEBUG("hardScatterJets size = " << hardScatterJets.size());
    for( const auto& jet: hardScatterJets){
      float ConstitJetPt=jet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum").pt();
      float PUsubtractedPt=jet->getAttribute<xAOD::JetFourMom_t>("JetPileupScaleMomentum").pt();
      //float FinalPt=jet->pt();
      //record the correction to the jet at that index:
      jet_cor.at(jet_index)=ConstitJetPt-PUsubtractedPt;
      jet_index++;
      jet_phi.push_back(jet->phi());
      m_jetphi.push_back(jet->phi());
      jet_eta.push_back(jet->eta());
      jet_area.push_back(jet->getAttribute<double>(xAOD::JetAttribute::AttributeID::ActiveArea));
     }
  }






  //####################################
  //######## Z->Mu+Mu Selection ########
  //####################################

  bool zmumu=false;

   int lead_min=25;
   int sublead_min=20;
   double eta_range=2.47;
   double muon_mass=0.1057;
   double mu1_z,mu1_x,mu1_y,mu1_e,mu2_z,mu2_x,mu2_y,mu2_e,z_px,z_py,z_pz,z_mass,z_energy;

   int charge_mult=m_mu1_Charge*m_mu2_Charge;
  
   
   if ((m_mu1_Pt>lead_min)&&(m_mu2_Pt>sublead_min)&&(charge_mult=-1)&&(m_mu1_Eta*m_mu1_Eta<eta_range*eta_range)&&(m_mu2_Eta*m_mu2_Eta<eta_range*eta_range)&&(m_mu1_MediumID==1)&&(m_mu2_MediumID==1)&&(m_mu1_GradientIso==1)&&(m_mu2_GradientIso==1)&&(m_nhard_muons==2)&&(mu1Zst<3)&&(mu2Zst<3)&&(mu1IDcuts*mu2IDcuts==1)){
     mu1_z=m_mu1_Pt*sinh(m_mu1_Eta);
     mu1_x=m_mu1_Pt*cos(m_mu1_Phi);
     mu1_y=m_mu1_Pt*sin(m_mu1_Phi);
     mu1_e=sqrt(mu1_x*mu1_x+mu1_y*mu1_y+mu1_z*mu1_z+muon_mass*muon_mass);
     
     mu2_z=m_mu2_Pt*sinh(m_mu2_Eta);
     mu2_x=m_mu2_Pt*cos(m_mu2_Phi);
     mu2_y=m_mu2_Pt*sin(m_mu2_Phi);
     mu2_e=sqrt(mu2_x*mu2_x+mu2_y*mu2_y+mu2_z*mu2_z+muon_mass*muon_mass);
     z_energy=mu1_e+mu2_e;
     z_px=mu1_x+mu2_x;
     z_py=mu1_y+mu2_y;
     z_pz=mu1_z+mu2_z;

     z_mass=sqrt(z_energy*z_energy-z_px*z_px-z_py*z_py-z_pz*z_pz);
     double z_diff=sqrt((z_mass-91.2)*(z_mass-91.2));
     if(z_diff<25)zmumu=true;
     
   }

   if(zmumu){
     m_zmumu=true;
     m_h_zmass->Fill(z_mass);
   }//zmumu loop

 


  //####################################
  //############ new PUfit##############
  //####################################
 
  
  if ((hardScatterJets.size()>0)&&(m_zmumu)){    
    // Calculate covariance of pileup deposits, collect into towers and calculate pileup 2-vector
    
    std::vector<Tower> pileupTowers(m_nTowers);
    TVector2 ptPileup;
    TVector2 ptHStrk;
    
    float v11 = 0.;
    float v12 = 0.;
    float v22 = 0.;
    double reso_floor=50;//0.5 GeV
    double reso_r=15.81;//in Mev^1/2
    for (const xAOD::CaloCluster* iclus : pileupClusters) {
      float sinPhi;
      float cosPhi;
      sincosf(iclus->phi(xAOD::CaloCluster::CALIBRATED), &sinPhi, &cosPhi);
      unsigned int etaBin = (iclus->eta(xAOD::CaloCluster::CALIBRATED) + m_maxEta)/(2*m_maxEta) * m_nEtaBins;
      unsigned int phiBin = fmod(iclus->phi(xAOD::CaloCluster::CALIBRATED)/TMath::TwoPi() + 1, 1) * m_nPhiBins;
      
      pileupTowers.at(etaBin*m_nPhiBins + phiBin) += *iclus;
      ptPileup += TVector2(cosPhi, sinPhi) *std::abs( iclus->pt(xAOD::CaloCluster::CALIBRATED));  
      
      double sigma2=reso_r*reso_r*std::abs( iclus->pt(xAOD::CaloCluster::CALIBRATED))+reso_floor;
      v11+=sigma2*cosPhi*cosPhi;
      v12+=sigma2*cosPhi*sinPhi;
      v22+=sigma2*sinPhi*sinPhi;
    }
    
    for (auto pv_track : pv_soft_tracks) {  
      float sinPhi;
      float cosPhi;
      sincosf(pv_track->phi(), &sinPhi, &cosPhi);
      unsigned int etaBin = (pv_track->eta() + m_maxEta)/(2*m_maxEta) * m_nEtaBins;
      unsigned int phiBin = fmod(pv_track->phi()/TMath::TwoPi() + 1, 1) * m_nPhiBins;
      auto index=etaBin*m_nPhiBins + phiBin;
      
      pileupTowers.at(index).px -= pv_track->pt()*cos(pv_track->phi());
      pileupTowers.at(index).py -=pv_track->pt()*sin(pv_track->phi());  
      pileupTowers.at(index).sumEt-=pv_track->pt();
      pileupTowers.at(index).hs_trk+=pv_track->pt();
      pileupTowers.at(index).trkx+=pv_track->pt()*cos(pv_track->phi());
      pileupTowers.at(index).trky+=pv_track->pt()*sin(pv_track->phi());
      ptPileup -= TVector2(cosPhi, sinPhi) * pv_track->pt();
      ptHStrk+=TVector2(cosPhi, sinPhi) * pv_track->pt();          
      
      double sigma2=reso_r*reso_r*pv_track->pt()+reso_floor;
      v11-=sigma2*cosPhi*cosPhi;
      v12-=sigma2*cosPhi*sinPhi;
      v22-=sigma2*sinPhi*sinPhi;
    }
    
    for (auto el : hard_el) {   
      float sinPhi;
      float cosPhi;
      sincosf(el->phi(), &sinPhi, &cosPhi);
      unsigned int etaBin = (el->eta() + m_maxEta)/(2*m_maxEta) * m_nEtaBins;
      unsigned int phiBin = fmod(el->phi()/TMath::TwoPi() + 1, 1) * m_nPhiBins;
      auto index=etaBin*m_nPhiBins + phiBin;
      pileupTowers.at(index).px -= el->pt()*cos(el->phi());
      pileupTowers.at(index).py -= el->pt()*sin(el->phi());   
      pileupTowers.at(index).sumEt-=el->pt();
      ptPileup -= TVector2(cosPhi, sinPhi) * el->pt();
      
      double sigma2=reso_r*reso_r*el->pt()+reso_floor;
      v11-=sigma2*cosPhi*cosPhi;
      v12-=sigma2*cosPhi*sinPhi;
      v22-=sigma2*sinPhi*sinPhi;
    } 
    
    
    for (auto pho : hard_photon) {   
      float sinPhi;
      float cosPhi;
      sincosf(pho->phi(), &sinPhi, &cosPhi);
      unsigned int etaBin = (pho->eta() + m_maxEta)/(2*m_maxEta) * m_nEtaBins;
      unsigned int phiBin = fmod(pho->phi()/TMath::TwoPi() + 1, 1) * m_nPhiBins;
      auto index=etaBin*m_nPhiBins + phiBin;
      
      pileupTowers.at(index).px -= pho->pt()*cos(pho->phi());
      pileupTowers.at(index).py -= pho->pt()*sin(pho->phi());   
      pileupTowers.at(index).sumEt-=pho->pt();
      ptPileup -= TVector2(cosPhi, sinPhi) * pho->pt();                            
      double sigma2=reso_r*reso_r*pho->pt()+reso_floor;
      v11-=sigma2*cosPhi*cosPhi;
      v12-=sigma2*cosPhi*sinPhi;
      v22-=sigma2*sinPhi*sinPhi; 
    } 
    
 
    
   
    double determinant=v11*v22-v12*v12;
    
    // first determine which towers overlap with HS jet:
    std::vector<Tower> high_towers;
    std::vector<Tower> low_towers;
    
    for (unsigned int ii = 0; ii < pileupTowers.size(); ++ii) {
      Tower& tower = pileupTowers.at(ii);
      // Calculate the (eta, phi) centre of the tower from its index
      unsigned int phiBin = ii % m_nPhiBins;
      float phi = (phiBin + 0.5) * m_towerPhiWidth;
      unsigned int etaBin = ii/m_nPhiBins;
      float eta = -m_maxEta + (etaBin + 0.5) * m_towerEtaWidth;
      pileupTowers.at(ii).phi=phi;
      pileupTowers.at(ii).eta=eta;
      bool overlap=false;// bool for low towers
      
      for (const xAOD::Jet* ijet : hardScatterJets) {
	double deltaPhi = fmod( TMath::TwoPi()+phi-ijet->phi() , TMath::TwoPi() );
	if (deltaPhi > M_PI) deltaPhi = TMath::TwoPi() - deltaPhi;
	deltaPhi=sqrt(deltaPhi*deltaPhi);
	double deltaEta = sqrt((eta-ijet->eta())*(eta-ijet->eta()));
	double jet_r=ijet->getSizeParameter();
	if((deltaPhi<(0.5*m_towerPhiWidth+jet_r))&&(deltaEta<(0.5*m_towerEtaWidth+jet_r))) overlap=true;
      }
      
      if(overlap){
	high_towers.push_back(pileupTowers.at(ii));
      }else{
	low_towers.push_back(pileupTowers.at(ii));
	//appending information:
	m_lowtower_phi.push_back(pileupTowers.at(ii).phi);
	m_lowtower_e.push_back(pileupTowers.at(ii).e_all());
	m_lowtower_p.push_back(pileupTowers.at(ii).hs_trk);
      }
    }
    
    double n_lowTower=low_towers.size();
    
    //calculate covariance matrix:
    float Px0=ptHStrk.Px();
    float Py0=ptHStrk.Py();
    float Ex0=ptPileup.Px()+Px0;
    float Ey0=ptPileup.Py()+Py0;
    
    //calculate density and tower variance:
    double omega0=1.;
    double var_omega=0.25;
    double area= m_towerPhiWidth* m_towerEtaWidth;
    
    //compute the nomalization factor g:
    double g=0;
    double psi_p=0; //constant part of psiP
    double psi_e=0; //constant part of psiE
    

    for (unsigned int ii = 0; ii < n_lowTower; ++ii){ 
      auto cur_tower=low_towers.at(ii);

      // g+=(1/n_lowTower)*(cur_tower.sumEt)/feta(cur_tower.eta,1);
      psi_e+=(1/n_lowTower)*(cur_tower.e_all())/feta(cur_tower.eta,1);
      psi_p+=(1/n_lowTower)*(cur_tower.hs_trk)/feta(cur_tower.eta,1);	
    }
    g=psi_e-omega0*psi_p;
    //compute normalization factor gv:
    double gv=0;
     for (unsigned int ii = 0; ii < n_lowTower; ++ii){ 
      auto cur_tower=low_towers.at(ii);
      double cur_var=(cur_tower.sumEt-feta(cur_tower.eta,g))*(cur_tower.sumEt-feta(cur_tower.eta,g));
      gv+=(1/n_lowTower)*cur_var/fveta(cur_tower.eta,1);
    }

    //compute the variance of each jet. And psiE psiP
     double jetArea=jet_area.at(0);
     double area_ratio=jetArea/area;
     std::vector<double> jet_var(hardScatterJets.size());
     std::vector<double> psiE(hardScatterJets.size());
     std::vector<double> psiP(hardScatterJets.size());
     for (unsigned int ii=0;ii<hardScatterJets.size();ii++){
       
       double cur_eta=jet_eta.at(ii);
       jet_var.at(ii)=fveta(cur_eta,gv)*area_ratio;
       // jet_var.at(ii)=0.001;
       psiP.at(ii)=feta(cur_eta,1)*area_ratio*psi_p;
       psiE.at(ii)=feta(cur_eta,1)*area_ratio*psi_e;
       //  std::cout<<"psi_P,PsiE,jetvar:"<<psiP.at(ii)<<";"<<psiE.at(ii)<<";"<<jet_var.at(ii)<<std::endl;
     }
     // std::cout<<"------------------------------"<<std::endl;


    
    //calculating matrix components:
    //associate a fitting parameter with each hardjets
    auto jetnum=hardScatterJets.size();
    std::vector<double> gamma(jetnum);
    std::vector<double> alpha(jetnum);
    std::vector<double> lambda(jetnum);	
    TMatrixD beta(jetnum,jetnum);
    
    for (unsigned int ii = 0; ii < hardScatterJets.size(); ++ii){
      double cosphi=cos(jet_phi.at(ii));
      double sinphi=sin(jet_phi.at(ii));
      
      alpha.at(ii)=(Ex0/determinant)*(v22*cosphi-v12*sinphi)+(Ey0/determinant)*(v11*sinphi-v12*cosphi)-psiE.at(ii)/jet_var.at(ii);
      gamma.at(ii)=(Px0/determinant)*(v22*cosphi-v12*sinphi)+(Py0/determinant)*(v11*sinphi-v12*cosphi)-psiP.at(ii)/jet_var.at(ii);
      lambda.at(ii)=(cosphi/determinant)*(v22*Px0-v12*Py0)+(sinphi/determinant)*(v11*Py0-v12*Px0)-psiP.at(ii)/jet_var.at(ii);
      for (unsigned int jj = 0; jj < hardScatterJets.size(); ++jj){
	double cosphi_2=cos(jet_phi.at(jj));
	double sinphi_2=sin(jet_phi.at(jj));
	beta[ii][jj]=(cosphi_2/determinant)*(v22*cosphi-v12*sinphi)+(sinphi_2/determinant)*(v11*sinphi-v12*cosphi);
      }
      
    }
    
      
    //first sum up the non-psi part, then sum up the psi part in a for loop
    double kappa=(Ex0/determinant)*(v22*Px0-v12*Py0)+(Ey0/determinant)*(v11*Py0-v12*Px0)+omega0/var_omega;
    double mu=(Px0/determinant)*(v22*Px0-v12*Py0)+(Py0/determinant)*(v11*Py0-v12*Py0)+1/var_omega;
    for (unsigned int ii = 0; ii < hardScatterJets.size(); ++ii){
      kappa+=psiP.at(ii)*psiE.at(ii)/jet_var.at(ii);
      mu+=psiP.at(ii)*psiP.at(ii)/jet_var.at(ii);
    }
    
    //finding solution matrix
    
    
    // Construct matrix to calculate corrections
    TMatrixD Xij(hardScatterJets.size()+1,hardScatterJets.size()+1);
    TMatrixD Cj(hardScatterJets.size()+1, 1);
    
    
    for (unsigned int ii = 0; ii < hardScatterJets.size(); ++ii) {
      Xij[ii][hardScatterJets.size()]=(-1)*gamma[ii];
      Xij[hardScatterJets.size()][ii]=lambda[ii];
      Cj[ii][0]=(-1)*alpha[ii];
      for (unsigned int jj = ii; jj <hardScatterJets.size(); ++jj) {
	Xij[ii][jj]=beta[ii][jj];
	if(ii==jj) Xij[ii][jj]+=(1/jet_var.at(ii));
	
      }
    }
    Xij[hardScatterJets.size()][hardScatterJets.size()]=(-1)*mu;
    Cj[hardScatterJets.size()][0]=(-1)*kappa;
    Xij.Invert();
    TMatrixD corrections(Xij * Cj);
    
    //calculating PAT
    double PATx=0.;
    double PATy=0.;
    double omega=corrections[hardScatterJets.size()][0];
    std::vector<double> expected_E(hardScatterJets.size());

     for (unsigned int ii=0;ii<hardScatterJets.size();ii++){
       double cur_eta=jet_eta.at(ii);
       // omega was taken to be one previously in the code for G, now we have determined omega, use it to get the better estimate of expected Energy:
       expected_E.at(ii)=psiE.at(ii)-omega*psiP.at(ii);
     }
 
    for (size_t ii=0;ii<hardScatterJets.size();ii++){
      PATx+=(corrections[ii][0]-expected_E.at(ii))*cos(jet_phi.at(ii));
      PATy+=(corrections[ii][0]-expected_E.at(ii))*sin(jet_phi.at(ii));
    }
    //ST:
    double STx=0.;
    double STy=0.;
    STx+=m_softex_TST*omega; // TST already has negative sign and in GeV;
    STy+=m_softey_TST*omega;
    
    m_PATx=PATx/1000.0;
    m_PATy=PATy/1000.0;
    m_STx=STx;
    m_STy=STy;
    m_omega=omega;
    m_Px0=Px0/1000.0;
    m_Py0=Py0/1000.0;
    m_Ex0=Ex0/1000.0;
    m_Ey0=Ey0/1000.0;
    
    m_nhightowers=(int)high_towers.size();

    //outlier debug:
    double pstx=m_PATx+m_STx;
    double psty=m_PATy+m_STy;
    double pst=mag(pstx,psty);
    if(pst>300){
      //first print out pst:
      std::cout<<"PST:"<<pst<<std::endl;
      std::cout<<"PATx="<<PATx<<std::endl;
      std::cout<<"PATy="<<PATy<<std::endl;
      std::cout<<"STx="<<STx<<std::endl;
      std::cout<<"STy="<<STy<<std::endl;
      std::cout<<"omega="<<m_omega<<std::endl;
      
      //print out jet location
      for (size_t ii=0;ii<hardScatterJets.size();ii++)	std::cout<<hardScatterJets.at(ii)->phi()<<std::endl;
      
      std::cout<<"Ex0="<<Ex0<<",Ey0="<<Ey0<<",Px0="<<Px0<<",Py0="<<Py0<<std::endl;
    }

  }//HS jet > 0
  m_nhardjets=(int)hardScatterJets.size();
 




  // fill the branches for outpur TTree
  m_tree->Fill();

  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode trigAthAnalysisAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}


