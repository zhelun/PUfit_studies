#ifndef TRIGATHANALYSIS_TRIGATHANALYSISALG_H
#define TRIGATHANALYSIS_TRIGATHANALYSISALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

// AnaToolHandle
#include <AsgTools/AnaToolHandle.h>
// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <PathResolver/PathResolver.h>
// jet tools
#include <JetInterface/IJetSelector.h>
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetCalibTools/IJetCalibrationTool.h"
// egamma tools
#include <EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h>
#include <EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h>
#include <EgammaAnalysisInterfaces/IAsgElectronIsEMSelector.h>
// muon tools
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
// isolation tools
#include <IsolationSelection/IIsolationSelectionTool.h>
// trigger tools
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
// MET maker
#include "METInterface/IMETMaker.h"
#include "METUtilities/CutsMETMaker.h" 
#include "METUtilities/METHelpers.h"

// ROOT Includes
#include "TTree.h"
#include "TH1.h"
#include "TH2.h"

//track selection
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"


class trigAthAnalysisAlg: public ::AthAnalysisAlgorithm { 
 public: 
  trigAthAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~trigAthAnalysisAlg(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virt
  //asg::AnaToolHandle<InDet::IInDetTrackSelectionTool>
  //m_trackSelectionTool{"InDet::InDetTrackSelectionTool"};
  //ual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 

   //Example algorithm property, see constructor for declaration:
   //int m_nProperty = 0;

  // GRL
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl{"GoodRunsListSelectionTool"};
  // jet tool
  asg::AnaToolHandle<IJetSelector> m_jetCleaning{"JetCleaningTool"}; 
  asg::AnaToolHandle<IJetCalibrationTool> m_jetCalibTool{"JetCalibrationTool"};
  // egamma tool
  asg::AnaToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_electronCalibrationAndSmearingTool{"CP::IEgammaCalibrationAndSmearingTool"};
  asg::AnaToolHandle<IAsgElectronLikelihoodTool> m_electronSelectionTool{"AsgElectronLikelihoodTool"};
  // muon tool
  asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool{"CP::MuonCalibrationAndSmearingTool"};
  asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool{"CP::MuonSelectionTool"};
  // isolation tool
  asg::AnaToolHandle<CP::IIsolationSelectionTool> m_isoSelectionTool{"CP::IsolationSelectionTool"};
  // trigger tools
  asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool{"Trig::TrigDecisionTool"};
  asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool{"TrigConf::xAODConfigTool"};
  // METMaker
  asg::AnaToolHandle<IMETMaker> m_metMaker{"met::METMaker"};


//trig selection tool
 // asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_trackSelectionTool{"InDet::InDetTrackSelectionTool"};


	asg::AnaToolHandle<InDet::IInDetTrackSelectionTool>
	m_trackSelectionTool{"InDet::InDetTrackSelectionTool"};





  // L1 trigger LUT related
  /** Number of bits in range */
  static const unsigned int m_nBits = 6;
  /** Number of ET ranges to encode in */
  static const unsigned int m_nRanges = 4;
  /** Mask to select Ex/Ey bits */
  static const unsigned int m_mask = 0x3F;
  /** Masks for integer MET word setting */
  static const unsigned int m_valueMask = 0x7F;
  static const unsigned int m_rangeMask = 0x180;
  /** Limits on inputs to XS LUT */
  static const int m_xsXEmax = 127;
  static const int m_xsSqrtTEmax  = 63;

  // number of events processed
  int m_nEvents;
  // number of events passed GRL
  int m_nEventsGRL;
  int m_doGRL;
  // number of events passed muon trigger
  int m_nEventsMu;
  int m_doMuTrigSelection;
  std::string m_trigMu1;
  std::string m_trigMu2;
  std::string m_trigMu3;
  std::string m_trigMu4;
  // number of events passed L1_XE trigger
  int m_nEventsL1;
  int m_doL1Selection;
  std::string m_trigL1pre;
  // container selection
  int m_do2016;
  int m_doClusters;
  int m_doTracks;
  int m_doCells;
  int m_doTrigTowers;
  std::string m_scale;

  // output histogram, see initialize method for registration to output histSvc
  TH1F* m_h_averageIPC = 0;
  TH1F* m_h_jetPt = 0;
  TH1F* m_h_elPt = 0;
  TH1F* m_h_photPt = 0;
  TH1F* m_h_muPt = 0;
  TH1F* m_h_tauPt = 0;
  TH1F* m_h_nClusters = 0;
  TH1F* m_h_clusterE = 0;
  TH1F* m_h_trackPt = 0;
  TH1F* m_h_cellE = 0;
  TH1F* m_h_cellEta = 0;
  TH1F* m_h_cellPhi = 0;
  TH1F* m_h_L1_Et = 0;
  TH1F* m_h_cell_Et = 0;
  TH1F* m_h_mht_Et = 0;
  TH1F* m_h_tclcw_Et = 0;
  TH1F* m_h_tclcw_pufit_Et = 0;
  TH1F* m_h_counts = 0;
  TH1F* m_h_MET_TST = 0;
  TH1F* m_h_MET_TST_noMu = 0;
  TH1F* m_h_MET_RefEle = 0;
  TH1F* m_h_MET_RefMuon = 0;
  TH1F* m_h_MET_RefJet = 0;
  TH1F* m_h_MET_TST_soft = 0;
  TH1F* m_h_nhard_track=0;
  TH1F* m_h_zmass=0;
   

  std::string m_outputName;
  TTree* m_tree = 0;

  int m_RunNumber;
  int m_EventNumber;
  int m_BCID;
  int m_LB;
  double m_mu;
  double m_muActual;
  int m_nVtx;
  int m_passMu;
  int m_passL1;
  int m_nJets;
  double m_jet1_Pt;
  int m_nElectrons;
  double m_el1_Pt;
  double m_el1_Eta;
  double m_el1_Phi;
  double m_el1_M;
  int m_el1_MediumID;
  int m_el1_GradientIso;
  double m_el2_Pt;
  double m_el2_Eta;
  double m_el2_Phi;
  double m_el2_M;
  int m_el2_MediumID;
  int m_el2_GradientIso;
  int m_nMuons;
  int m_nhard_muons;
   double m_mu1_Pt; //!
  double m_mu1_Eta; //!
  double m_mu1_Phi; //!
  double m_mu1_M; //!
  int m_mu1_Charge;
  int m_mu1_MediumID; //!
  int m_mu1_GradientIso; //!
  double m_mu2_Pt; //!
  double m_mu2_Eta; //!
  double m_mu2_Phi; //!
  double m_mu2_M; //!
  int m_mu2_Charge;
  int m_mu2_MediumID; //!
  int m_mu2_GradientIso; //!
  double m_mu3_Pt; //!
  double m_mu3_Eta; //!
  double m_mu3_Phi; //!
  double m_mu3_M; //!
  int m_mu3_Charge;
  int m_mu3_MediumID; //!
  int m_mu3_GradientIso; //!
  double m_MET_cellEmulate;
  double m_Ex_cellEmulate;
  double m_Ey_cellEmulate;
  double m_sumEt_cellEmulate;
  double m_MET_L1;
  double m_Ex_L1;
  double m_Ey_L1;
  double m_MET_cell;
  double m_phi_cell;
  double m_Ex_cell;
  double m_Ey_cell;
  double m_Ez_cell;
  double m_sumEt_cell;
  double m_MET_mht;
  double m_phi_mht;
  double m_Ex_mht;
  double m_Ey_mht;
  double m_Ez_mht;
  double m_sumEt_mht;
  double m_MET_pufit;
  double m_phi_pufit;
  double m_Ex_pufit;
  double m_Ey_pufit;
  double m_Ez_pufit;
  double m_sumEt_pufit;
  double m_MET_offline;
  double m_phi_offline;
  double m_Ex_offline;
  double m_Ey_offline;
  double m_MET_offlineNoMu;
  double m_Ex_offlineNoMu;
  double m_Ey_offlineNoMu;

 float m_size_parameter;

  double m_exEl;
  double m_eyEl;
  double m_El_met;
  double m_exMu;
  double m_eyMu;
  double m_Mu_met;
  double m_exJet;
  double m_eyJet;
  double m_Jet_met;


  double m_softmet_TST;
  double m_softex_TST;
  double m_softey_TST;
  double m_softmet_CST;
 

  double m_raw_jetx;
  double m_raw_jety;
  double m_raw_elx;
  double m_raw_ely;
  double m_raw_photonx;
  double m_raw_photony;
  double m_raw_muonx;
  double m_raw_muony;
  double m_raw_taux;
  double m_raw_tauy;
  double m_raw_tstx;
  double m_raw_tsty; 
  double m_softex_CST;
  double m_softey_CST;

  double m_PATx;
  double m_PATy;
  double m_STx;
  double m_STy;
  double m_omega;

  double m_Ex0;
  double m_Ey0;
  double m_Px0;
  double m_Py0;
  std::vector<double> m_jetphi;	
  std::vector<double> m_lowtower_phi;
  std::vector<double> m_lowtower_e;
  std::vector<double> m_lowtower_p;

  bool m_zmumu;
  int m_nhardjets;
  int m_nhightowers;	

  // pufit
  // Configurables
  double m_targetTowerWidth;
  double m_maxEta;
  double m_caloResSqrtTerm;
  double m_caloResFloor;
  double m_nSigma;
  double m_constraintWeight;
  double m_trimFactor;
  // Internals
  unsigned int m_nPhiBins;
  unsigned int m_nEtaBins;
  unsigned int m_nTowers;
  double m_towerEtaWidth;
  double m_towerPhiWidth;


  std::vector<double> m_jet_expected_PU;

  // L1 trigger names
  std::string m_trigL1;
  std::string m_trigL1KF;
  int m_nTrigL1;
  int m_nTrigL1KF;
  float m_pTrigL1;
  float m_pTrigL1KF;
  // HLT trigger names
  std::string m_trig1;
  std::string m_trig2;
  std::string m_trig3;
  std::string m_trig4;
  std::string m_trig5;
  std::string m_trig6;
  std::string m_trig7;
  std::string m_trig8;
  std::string m_trig9;
  std::string m_trig10;
  // number of events pass the trigger
  int m_nTrig1;
  int m_nTrig2;
  int m_nTrig3;
  int m_nTrig4;
  int m_nTrig5;
  int m_nTrig6;
  int m_nTrig7;
  int m_nTrig8;
  int m_nTrig9;
  int m_nTrig10;
  float m_pTrig1;
  float m_pTrig2;
  float m_pTrig3;
  float m_pTrig4;
  float m_pTrig5;
  float m_pTrig6;
  float m_pTrig7;
  float m_pTrig8;
  float m_pTrig9;
  float m_pTrig10;
  // L1 ROI name
  std::string m_L1ROI;
  int m_nL1;
  float m_shresholdL1;
  int m_nL1Thresh;
  // HLT trigger container names
  std::string m_alg1;
  std::string m_alg2;
  std::string m_alg3;
  std::string m_alg4;
  std::string m_alg5;
  std::string m_alg6;
  std::string m_alg7;
  std::string m_alg8;
  // number of events in the container
  int m_nAlg1;
  int m_nAlg2;
  int m_nAlg3;
  int m_nAlg4;
  int m_nAlg5;
  int m_nAlg6;
  int m_nAlg7;
  int m_nAlg8;
  // number of events pass the L1 thresholds
  int m_nAlgL1Thresh1;
  int m_nAlgL1Thresh2;
  int m_nAlgL1Thresh3;
  int m_nAlgL1Thresh4;
  int m_nAlgL1Thresh5;
  int m_nAlgL1Thresh6;
  int m_nAlgL1Thresh7;
  int m_nAlgL1Thresh8;
  // algorithm HLT thresholds
  float m_shreshold1;
  float m_shreshold2;
  float m_shreshold3;
  float m_shreshold4;
  float m_shreshold5;
  float m_shreshold6;
  float m_shreshold7;
  float m_shreshold8;
  // number of events pass the HLT thresholds
  int m_nAlgThresh1;
  int m_nAlgThresh2;
  int m_nAlgThresh3;
  int m_nAlgThresh4;
  int m_nAlgThresh5;
  int m_nAlgThresh6;
  int m_nAlgThresh7;
  int m_nAlgThresh8;

}; 

#endif //> !TRIGATHANALYSIS_TRIGATHANALYSISALG_H
